import numpy as np
from evaluation import fid
import tflib.Datasets_tff
import tensorflow as tf
import argparse
from os import listdir
from os.path import isfile, join
from tqdm import tqdm
#init_inception_net
inception_path = fid.check_or_download_inception('evaluation/inception_model') # download inception network
fid.create_inception_graph(inception_path) # load the graph into the current TF graph


def compute_fid( real_dataset_name, fake_data_vec, label ):
	np.random.seed(1234)
	if real_dataset_name == 'MB-c': #mnist-background composite
		c_dataset = tflib.Datasets_tff.MnistBackground_datasets(100, 100)
		c_dataset = c_dataset.train_data[0] #composites
	elif real_dataset_name == 'MB-f':
		c_dataset = tflib.Datasets_tff.MnistBackground_datasets(100, 100)
		c_dataset = c_dataset.train_data[2] #foreground		
	elif real_dataset_name == 'FMB-f':
		if label == -1:
			c_dataset = tflib.Datasets_tff.FashionMnistBackground_datasets(100, 100)
			c_dataset = c_dataset.train_data[2] #foreground	
		else:
			c_dataset = tflib.Datasets_tff.FashionMnistBackground_datasets(100, 100, label_ind_dataset=True)
			c_dataset = c_dataset.train_data_dict[label][2]
	elif real_dataset_name == 'FMB-c':
		c_dataset = tflib.Datasets_tff.FashionMnistBackground_datasets(100, 100)
		c_dataset = c_dataset.train_data[0] #composites		
	fid_vec = np.zeros((len(fake_data_vec)))
	with tf.Session() as sess:
		sess.run(tf.global_variables_initializer())
		for i in tqdm(xrange(len(fake_data_vec))):
			#load generated images
			fake_imgs = np.load( fake_data_vec[i] )
			sample_size = min(fake_imgs.shape[0], c_dataset.shape[0])
			fake_imgs = fake_imgs[:sample_size]
			#sampling from c_dataset
			rand_idx = np.random.permutation(sample_size)
			real_imgs = (c_dataset[rand_idx]*256).astype('uint8')
			real_imgs = np.reshape(real_imgs, [-1, 28, 28, 1]) 
			real_imgs = np.tile(real_imgs, [1, 1, 1, 3]) 

			mu_real, sigma_real = fid.calculate_activation_statistics(real_imgs, sess, batch_size=100)
			mu_fake, sigma_fake = fid.calculate_activation_statistics(fake_imgs, sess, batch_size=100)
			fid_vec[i] = fid.calculate_frechet_distance(mu_fake, sigma_fake, mu_real, sigma_real)
	return fid_vec

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='computing fid from generated images')
	parser.add_argument(
		"-dataset",
		help="which dataset: MB-c(mnist-background-composite), MB-f(mnist-background-foreground), FMB-f(fashion-mnist-foreground), FMB-c(fashion-mnist-composite)",
		required=True
	)
	parser.add_argument(
		"-genpath",
		help="generated image folder",
		required=True

	)
	parser.add_argument(
		"-label",
		help="specify class of the dataset, using only that class",
		type=int,
		default=-1
	)
	args = parser.parse_args()
	files_vec = [ join( args.genpath, f ) for f in listdir(args.genpath) if isfile(join(args.genpath, f)) ]
	fid_vec = compute_fid( args.dataset, files_vec, args.label)
	with open(join(args.genpath, 'fid.npy'), 'w' ) as f:
		np.save(f, fid_vec)
	print( 'fid vec: = {}'.format(fid_vec))
	print( 'mean = {:.3f}, std = {:.3f}'.format(np.mean(fid_vec), np.std(fid_vec)) )
