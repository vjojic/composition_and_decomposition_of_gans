import tflib.Datasets_tff
import tflib.ops.linear
import tflib.ops.deconv2d
import tflib.ops.conv2d
import tflib.ops.batchnorm
import tflib.save_images
import tensorflow as tf
import numpy as np
import dateutil.tz
import datetime
import getpass
import tflib.plot
import os
import matplotlib
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from timeit import default_timer as timer
import collections
from tensorflow.contrib.training import HParams 
import json
import argparse

def read_config(file_path):
    """Read JSON config."""
    json_object = json.load(open(file_path, 'r'))
    return json_object

parser = argparse.ArgumentParser(description='train composition/decomposition GAN model')
parser.add_argument(
    "-setting",
    help="path to the config file of all parameters (json file)",
    required=True
)
parser.add_argument(
    "-sampling",
    help="if ony sampling from the given model",
    dest='sampling', 
    action='store_true'
)
parser.add_argument(
    "-output",
    help="path where the program save outputs",
    dest='output', 
    default=""
)

args = parser.parse_args()
settings = read_config(args.setting)

print('-'*10+'Input args'+'-'*10)
print(args)
print('-'*30)

if args.sampling:
    SamplingConfig = collections.namedtuple('Configuration', [
    'sampling_times', 'sampling_rounds', 'sampling_outfile_name'])
    sampling_configs = SamplingConfig(sampling_times=settings['sampling_times'],
                                      sampling_rounds=settings['sampling_rounds'],
                                      sampling_outfile_name=settings['sampling_outfile_name'])

Configuration = collections.namedtuple('Configuration', [
    'eval_only', 'comp_model_path', 'decomp_model_path', 
    'fg_model_path', 'bg_model_path',
    'output_dim', 'v_size', 
    'train_comp', 'train_decomp',
    'train_fg', 'train_bg',
    'real_fg', 'real_bg', 
    'comp_use_side_info', 'decomp_use_side_info',
    'fg_Gnoise', 'dataset', 'label_used'
])
configs = Configuration(eval_only=settings['eval_only'], 
                        #comp_model_path='/home/ycharn/mnist-background/comp_decomp/2018_07_05_16_51_22/comp', 
                        comp_model_path=settings['pretrained_model_paths']['comp_model_path'], #the path to the pretrained composition model
                        decomp_model_path=settings['pretrained_model_paths']['decomp_model_path'], #the path to the pretrained decomposition model
                        fg_model_path=settings['pretrained_model_paths']['fg_model_path'], #the path to the pretrained foreground model
                        bg_model_path=settings['pretrained_model_paths']['bg_model_path'], #the path to the pretrained background model
                        output_dim=settings['data']['output_dim'], #28*28 for mnist-background, 64*64 for mnist-background-box3-noscale
                        v_size=settings['v_size'], #size of images for visualization at once
                        train_comp=settings['training_objective']['train_comp'], #whether to train compostion model
                        train_decomp=settings['training_objective']['train_decomp'], #whether to train decompostion model,
                        train_fg=settings['training_objective']['train_fg'], #whether to train foreground generator
                        train_bg=settings['training_objective']['train_bg'], #whether to train background generator
                        real_fg=settings['real_fg'], #whether to use real foreground data, used for training composition model
                        real_bg=settings['real_fg'], #whether to use background data, , used for training composition model
                        comp_use_side_info=settings['comp_use_side_info'], #use side information when training comp
                        decomp_use_side_info=settings['decomp_use_side_info'], #use side information when training decomp,
                        fg_Gnoise=settings['data']['fg_Gnoise'], #apply noise on foreground when using mnist-background dataset
                        dataset=settings['data']['dataset'], #mnist-background, mnist-background-box1, mnist-background-box2, mnist-background-box3, mnist-background-box3-noscale
                        label_used=settings['data']['label_used'], #used label in mnist-background dataset, -1 = using all
                        )
#regularize some configs
if configs.dataset == 'mnist-background' or configs.dataset == 'fashion-mnist-background':
    configs=configs._replace(output_dim=28*28)
elif configs.dataset == 'mnist-background-box2' or \
   configs.dataset == 'mnist-background-box3' or \
   configs.dataset == 'mnist-background-box3-noscale':
   configs=configs._replace(output_dim=64*64)
if configs.train_comp and configs.train_decomp:
    configs = configs._replace(comp_use_side_info=False, decomp_use_side_info=False)
if configs.train_fg:
    configs = configs._replace(real_fg=False)
if configs.train_bg:
    configs = configs._replace(real_bg=False)
if configs.fg_model_path:
    configs = configs._replace(real_fg=False)
if configs.bg_model_path:
    configs = configs._replace(real_bg=False)
DATA_LEN = int(np.sqrt(configs.output_dim))
hparams = HParams(batch_size=settings['hyperparams']['batch_size'], #batch size for training
                  LAMBDA=settings['hyperparams']['LAMBDA'],
                  iters=settings['hyperparams']['iters'],
                  disc_iters=settings['hyperparams']['disc_iters'],
                  comp_disc_dim=settings['hyperparams']['comp_disc_dim'],
                  comp_gen_dim=settings['hyperparams']['comp_gen_dim'],
                  decomp_disc_dim=settings['hyperparams']['decomp_disc_dim'],
                  decomp_gen_dim=settings['hyperparams']['decomp_disc_dim'],
                  noise_type=settings['hyperparams']['noise_type'], #Gaussian, Uniform
                  disc_learning_rate=settings['hyperparams']['disc_learning_rate'],
                  gen_learning_rate=settings['hyperparams']['gen_learning_rate'],
                  cycle_loss_p=settings['hyperparams']['cycle_loss_p']
                  )
fg_hparams = HParams(batch_size=settings['fg_hyperparams']['batch_size'],
                     code_size=settings['fg_hyperparams']['code_size'],
                     gen_dim=settings['fg_hyperparams']['gen_dim'],
                     noise_type=settings['fg_hyperparams']['noise_type']
                     )
bg_hparams = HParams(batch_size=settings['bg_hyperparams']['batch_size'],
                     code_size=settings['bg_hyperparams']['code_size'],
                     gen_dim=settings['bg_hyperparams']['gen_dim'],
                     noise_type=settings['bg_hyperparams']['noise_type']
                     )

c_user_name = getpass.getuser()
now = datetime.datetime.now(dateutil.tz.tzlocal())
timestamp = now.strftime('%Y_%m_%d_%H_%M_%S')
if args.output == '':
    out_basedir = "/home/{}/{}/comp_decomp/".format(c_user_name, configs.dataset) + timestamp #folder to output model and generated images
else:
    out_basedir = "{}".format(args.output)# + timestamp 
print('output root folder = {}'.format(out_basedir))
if not os.path.exists(out_basedir):
    os.makedirs(out_basedir)

print('-'*10+'Configurations'+'-'*10)
print(configs)
print('-'*30)
print('-'*10+'composition/decomposition operators'' hyper-parameters'+'-'*10)
print(hparams)
print('-'*30)
print('-'*10+'foreground generator hyper-parameters'+'-'*10)
print(fg_hparams)
print('-'*30)
print('-'*30)
print('-'*10+'background generator hyper-parameters'+'-'*10)
print(bg_hparams)
print('-'*30)

if configs.dataset == 'mnist-background':
    if configs.label_used == -1:
        MnistB = tflib.Datasets_tff.MnistBackground_datasets(hparams.batch_size, hparams.batch_size, configs.fg_Gnoise)
    else:
        MnistB = tflib.Datasets_tff.MnistBackground_datasets(hparams.batch_size, hparams.batch_size, configs.fg_Gnoise, label_ind_dataset=True)
elif configs.dataset == 'fashion-mnist-background':
    if configs.label_used == -1:
        MnistB = tflib.Datasets_tff.FashionMnistBackground_datasets(hparams.batch_size, hparams.batch_size, configs.fg_Gnoise)
    else:
        MnistB = tflib.Datasets_tff.FashionMnistBackground_datasets(hparams.batch_size, hparams.batch_size, configs.fg_Gnoise, label_ind_dataset=True)
elif configs.dataset == 'mnist-backgroundinv':
    if configs.label_used == -1:
        MnistB = tflib.Datasets_tff.MnistBackgroundInv_datasets(hparams.batch_size, hparams.batch_size, configs.fg_Gnoise)
    else:
        MnistB = tflib.Datasets_tff.MnistBackgroundInv_datasets(hparams.batch_size, hparams.batch_size, configs.fg_Gnoise, label_ind_dataset=True)    
elif configs.dataset == 'mnist-background-box1':
    MnistB = tflib.Datasets_tff.MnistBackgroundBox1_datasets(hparams.batch_size, hparams.batch_size)
elif configs.dataset == 'mnist-background-box2':
    MnistB = tflib.Datasets_tff.MnistBackgroundBox2_datasets(hparams.batch_size, hparams.batch_size)
elif configs.dataset == 'mnist-background-box3':
    MnistB = tflib.Datasets_tff.MnistBackgroundBox3_datasets(hparams.batch_size, hparams.batch_size)
elif configs.dataset == 'mnist-background-box3-noscale':
    if configs.label_used == -1:
        MnistB = tflib.Datasets_tff.MnistBackgroundBox3_datasets(hparams.batch_size, hparams.batch_size, scale_dataset=False)
    else:
        MnistB = tflib.Datasets_tff.MnistBackgroundBox3_datasets(hparams.batch_size, hparams.batch_size, scale_dataset=False, label_ind_dataset=True)

def load_model(model_folder, target_saver):
    cwd = os.getcwd()
    if not os.path.isdir(model_folder):
        print('cannot find model in {}'.format(model_folder))
    else:
        os.chdir(model_folder)
        target_saver.restore(session,'model_snapshot')
        os.chdir(cwd)
def save_model(model_folder, target_saver):
    cwd = os.getcwd()
    if not os.path.exists(model_folder):
        os.makedirs(model_folder)
    os.chdir(model_folder)
    target_saver.save(session, './model_snapshot')            
    os.chdir(cwd)

def custom_concat(axis, input):
    if tf.__version__[0] == '0':
        return tf.concat(axis, input)
    elif tf.__version__[0] == '1':
        return tf.concat(input, axis) 

def LeakyReLU(x, alpha=0.2):
    return tf.maximum(alpha*x, x)

def Discriminator32(inputs, name, dim, channel_size):
    output = tf.reshape(inputs, [-1, channel_size, DATA_LEN, DATA_LEN])

    output = tflib.ops.conv2d.Conv2D(name+'_layer.1',channel_size, dim,5,output,stride=2)
    output = LeakyReLU(output)

    output = tflib.ops.conv2d.Conv2D(name+'_layer.2', dim, 2*dim, 5, output, stride=2)
    output = LeakyReLU(output)

    output = tflib.ops.conv2d.Conv2D(name+'_layer.3', 2*dim, 4*dim, 5, output, stride=2)
    output = LeakyReLU(output)

    output = tf.reshape(output, [-1, 4*4*4*dim])
    output = tflib.ops.linear.Linear(name+'_Output', 4*4*4*dim, 1, output)

    return tf.reshape(output, [-1])

def Discriminator64(inputs, name, dim, channel_size):
    output = tf.reshape(inputs, [-1, channel_size, DATA_LEN, DATA_LEN])

    output = tflib.ops.conv2d.Conv2D(name+'_layer.1',channel_size, dim, 5, output, stride=2)
    output = LeakyReLU(output)

    output = tflib.ops.conv2d.Conv2D(name+'_layer.2', dim, 2*dim, 5, output, stride=2)
    output = LeakyReLU(output)

    output = tflib.ops.conv2d.Conv2D(name+'_layer.3', 2*dim, 4*dim, 5, output, stride=2)
    output = LeakyReLU(output)

    output = tflib.ops.conv2d.Conv2D(name+'_layer.4', 4*dim, 8*dim, 5, output, stride=2)
    output = LeakyReLU(output)

    output = tf.reshape(output, [-1, 4*4*8*dim])
    output = tflib.ops.linear.Linear(name+'_Output', 4*4*8*dim, 1, output)

    return tf.reshape(output, [-1])

def d2Generator32(inputs, name, dim=32, input_channel_size=1):
    output = tf.reshape(inputs, [-1, input_channel_size, DATA_LEN, DATA_LEN])

    down1 = tflib.ops.conv2d.Conv2D(name+'_CL.1',input_channel_size, dim,5,output,stride=2) 
    down1 = LeakyReLU(down1) #32*14*14

    down2 = tflib.ops.conv2d.Conv2D(name+'_CL.2', dim, 2*dim, 5, down1, stride=2)
    down2 = LeakyReLU(down2) #64*7*7

    down3 = tflib.ops.conv2d.Conv2D(name+'_CL.3', 2*dim, 4*dim, 5, down2, stride=2)
    down3 = LeakyReLU(down3) #128*4*4

    up1 = tflib.ops.deconv2d.Deconv2D(name+'_DL.1', 4*dim, 2*dim, 5, down3)
    up1 = tf.nn.relu(up1) #64*8*8


    up1 = up1[:,:,:7,:7] #64*7*7
    #up1 = custom_concat(1, [down2, up1]) #skip connection #128*7*7
    up2 = tflib.ops.deconv2d.Deconv2D(name+'_DL.2', 2*dim, dim, 5, up1)
    up2 = tf.nn.relu(up2) #32*14*14
    #output = tflib.ops.deconv2d.Deconv2D('Generator.3', 2*DIM, 1, 5, output, strides=[1,4,4,1])

    #up2 = custom_concat(1, [down1, up2]) #64*14*14
    up3 = tflib.ops.deconv2d.Deconv2D(name+'_DL.3', dim, 2, 5, up2)
    up3 = tf.nn.sigmoid(up3)

    return tf.reshape(up3, [-1, 2*configs.output_dim])

def d2Generator32_2(inputs, name, dim=32, input_channel_size=1):
    output = tf.reshape(inputs, [-1, input_channel_size, DATA_LEN, DATA_LEN])

    down1 = tflib.ops.conv2d.Conv2D(name+'_CL.1',input_channel_size, dim, 5,output,stride=1) 
    down1 = LeakyReLU(down1) #32*28*28
    down2 = tflib.ops.conv2d.Conv2D(name+'_CL.2', dim, 2*dim, 5, down1, stride=2)
    down2 = LeakyReLU(down2) #64*14*14
    down3 = tflib.ops.conv2d.Conv2D(name+'_CL.3', 2*dim, 4*dim, 5, down2, stride=2)
    down3 = LeakyReLU(down3) #128*7*7
    down4 = tflib.ops.conv2d.Conv2D(name+'_CL.4', 4*dim, 4*dim, 5, down3, stride=2)
    down4 = LeakyReLU(down4) #128*4*4

    up1 = tflib.ops.deconv2d.Deconv2D(name+'_DL.1', 4*dim, 4*dim, 5, down4)
    up1 = tf.nn.relu(up1) #4*dim*8*8
    up1 = up1[:,:,:7,:7] #7*7
    #up1 = custom_concat(1, [down3, up1]) #skip connection #128*7*7
    up1 = up1 + down3
    up2 = tflib.ops.deconv2d.Deconv2D(name+'_DL.2', (4)*dim, 2*dim, 5, up1)
    up2 = tf.nn.relu(up2) #14*14
    #up2 = custom_concat(1, [down2, up2]) 
    up2 = up2 + down2
    up3 = tflib.ops.deconv2d.Deconv2D(name+'_DL.3', (2)*dim, dim, 5, up2)
    up3 = tf.nn.relu(up3) #28*28
    #up3 = custom_concat(1, [down1, up3])
    up3 = up3 + down1
    up3 = tflib.ops.conv2d.Conv2D(name+'_DL.4', (1)*dim, 2, 5, up3, stride=1)
    up3 = tf.nn.sigmoid(up3)

    return tf.reshape(up3, [-1, 2*configs.output_dim])

def d2Generator64(inputs, name, dim=32, channel_size=1,):
    output = tf.reshape(inputs, [-1, channel_size, DATA_LEN, DATA_LEN])

    down1 = tflib.ops.conv2d.Conv2D(name+'_layer.1', channel_size, dim, 5, output, stride=2)
    down1 = tf.nn.relu(down1)
    #down1 = tf.nn.max_pool(down1, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    down2 = tflib.ops.conv2d.Conv2D(name+'_layer.2', dim, 2*dim, 5, down1, stride=2)
    down2 = tf.nn.relu(down2)
    #down2 = tf.nn.max_pool(down2, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    down3 = tflib.ops.conv2d.Conv2D(name+'_layer.3', 2*dim, 4*dim, 5, down2, stride=2)
    down3 = tf.nn.relu(down3)
    #down3 = tf.nn.max_pool(down3, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    down4 = tflib.ops.conv2d.Conv2D(name+'_layer.4', 4*dim, 8*dim, 5, down3, stride=2)
    down4 = tf.nn.relu(down4)
    #down4 = tf.nn.max_pool(down4, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    up1 = tflib.ops.deconv2d.Deconv2D(name+'_layer.5', 8*dim, 4*dim, 5, down4)
    up1 = tf.nn.relu(up1)

    up1 = custom_concat(1, [down3, up1]) #skip connection
    up2 = tflib.ops.deconv2d.Deconv2D(name+'._layer.6', 2*4*dim, 2*dim, 5, up1)
    up2 = tf.nn.relu(up2)

    up2 = custom_concat(1, [down2, up2]) #skip connection
    up3 = tflib.ops.deconv2d.Deconv2D(name+'._layer.7', 2*2*dim, dim, 5, up2)
    up3 = tf.nn.relu(up3)

    up3 = custom_concat(1, [down1, up3]) #skip connection
    up4 = tflib.ops.deconv2d.Deconv2D(name+'.layer.8', 2*dim, 2, 5, up3)
    up4 = tf.nn.sigmoid(up4)

    return tf.reshape(up4, [-1, 2*configs.output_dim])

def cGenerator1(inputs, name, dim=32, channel_size=2):
    output = tf.reshape(inputs, [-1, channel_size, DATA_LEN, DATA_LEN])

    output = tflib.ops.conv2d.Conv2D(name+'_layer.1', channel_size, dim, 3, output, stride=1)
    output = tf.nn.relu(output)

    output = tflib.ops.conv2d.Conv2D(name+'_layer.2', dim, 1, 3, output, stride=1)
    output = tf.nn.sigmoid(output)

    return tf.reshape(output, [-1, configs.output_dim])

def cGenerator1_2(inputs, name, dim=32, channel_size=2):
    output = tf.reshape(inputs, [-1, channel_size, DATA_LEN, DATA_LEN])

    output = tflib.ops.conv2d.Conv2D(name+'_layer.1', channel_size, dim, 3, output, stride=1)
    output = tf.nn.relu(output)

    output = tflib.ops.conv2d.Conv2D(name+'_layer.2', dim, dim, 3, output, stride=1)
    output = tf.nn.relu(output)

    output = tflib.ops.conv2d.Conv2D(name+'_layer.3', dim, dim, 3, output, stride=1)
    output = tf.nn.relu(output)

    output = tflib.ops.conv2d.Conv2D(name+'_layer.4', dim, 1, 3, output, stride=1)
    output = tf.nn.sigmoid(output)

    return tf.reshape(output, [-1, configs.output_dim])    

def cGenerator2(inputs, name, dim=32, channel_size=2):
    output = tf.reshape(inputs, [-1, channel_size, DATA_LEN, DATA_LEN]) #bx2x28x28

    out1 = tflib.ops.conv2d.Conv2D(name+'_layer.1', channel_size, dim, 5, output, stride=1) #bxdimx28x28
    out1 = tf.nn.relu(out1)

    out1 = custom_concat(1, [out1, output])
    out2 = tflib.ops.conv2d.Conv2D(name+'_layer.2', dim+2, dim, 5, out1, stride=1) #bxdimx28x28
    out2 = tf.nn.relu(out2)

    out2 = custom_concat(1, [out1, out2])
    out3 = tflib.ops.conv2d.Conv2D(name+'_layer.3', 2*dim+2, 1, 5, out2, stride=1) #bx1x28x28

    out3 = tf.nn.sigmoid(out3)

    return tf.reshape(out3, [-1, configs.output_dim])


def cGenerator32(inputs, name, dim=32, channel_size=2):
    output = tf.reshape(inputs, [-1, channel_size, DATA_LEN, DATA_LEN])

    output = tflib.ops.conv2d.Conv2D(name+'_layer.1', channel_size, dim, 5, output, stride=2)
    output = tf.nn.relu(output)

    output = tflib.ops.conv2d.Conv2D(name+'_layer.2', dim, dim*2, 5, output, stride=2)
    output = tf.nn.relu(output)

    output = tflib.ops.conv2d.Conv2D(name+'_layer.3', dim*2, dim*4, 5, output, stride=2)
    output = tf.nn.relu(output)

    output = tflib.ops.deconv2d.Deconv2D(name+'._layer.4', 4*dim, 2*dim, 5, output)
    output = tf.nn.relu(output)
    output = output[:,:,:7,:7]

    output = tflib.ops.deconv2d.Deconv2D(name+'._layer.5', 2*dim, dim, 5, output)
    output = tf.nn.relu(output)

    output = tflib.ops.deconv2d.Deconv2D(name+'.layer.6', dim, 1, 5, output)
    output = tf.nn.sigmoid(output)

    return tf.reshape(output, [-1, configs.output_dim])

def cGenerator64(inputs, name, dim=32, channel_size=2):
    output = tf.reshape(inputs, [-1, channel_size, DATA_LEN, DATA_LEN])

    down1 = tflib.ops.conv2d.Conv2D(name+'_layer.1', channel_size, dim, 5, output, stride=2)
    down1 = tf.nn.relu(down1)
    #down1 = tf.nn.max_pool(down1, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    down2 = tflib.ops.conv2d.Conv2D(name+'_layer.2', dim, 2*dim, 5, down1, stride=2)
    down2 = tf.nn.relu(down2)
    #down2 = tf.nn.max_pool(down2, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    down3 = tflib.ops.conv2d.Conv2D(name+'_layer.3', 2*dim, 4*dim, 5, down2, stride=2)
    down3 = tf.nn.relu(down3)
    #down3 = tf.nn.max_pool(down3, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    down4 = tflib.ops.conv2d.Conv2D(name+'_layer.4', 4*dim, 8*dim, 5, down3, stride=2)
    down4 = tf.nn.relu(down4)
    #down4 = tf.nn.max_pool(down4, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    up1 = tflib.ops.deconv2d.Deconv2D(name+'_layer.5', 8*dim, 4*dim, 5, down4)
    up1 = tf.nn.relu(up1)

    up2 = tflib.ops.deconv2d.Deconv2D(name+'._layer.6', 4*dim, 2*dim, 5, up1)
    up2 = tf.nn.relu(up2)

    up3 = tflib.ops.deconv2d.Deconv2D(name+'._layer.7', 2*dim, dim, 5, up2)
    up3 = tf.nn.relu(up3)

    up4 = tflib.ops.deconv2d.Deconv2D(name+'.layer.8', dim, 1, 5, up3)
    up4 = tf.nn.sigmoid(up4)

    return tf.reshape(up4, [-1, 1*configs.output_dim])

def cGenerator4(inputs, name, dim=32, channel_size=2):
    output = tf.reshape(inputs, [-1, channel_size, DATA_LEN, DATA_LEN])

    down1 = tflib.ops.conv2d.Conv2D(name+'_layer.1', channel_size, dim, 5, output, stride=2)
    down1 = tf.nn.relu(down1)
    #down1 = tf.nn.max_pool(down1, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    down2 = tflib.ops.conv2d.Conv2D(name+'_layer.2', dim, 2*dim, 5, down1, stride=2)
    down2 = tf.nn.relu(down2)
    #down2 = tf.nn.max_pool(down2, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    down3 = tflib.ops.conv2d.Conv2D(name+'_layer.3', 2*dim, 4*dim, 5, down2, stride=2)
    down3 = tf.nn.relu(down3)
    #down3 = tf.nn.max_pool(down3, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    down4 = tflib.ops.conv2d.Conv2D(name+'_layer.4', 4*dim, 8*dim, 5, down3, stride=2)
    down4 = tf.nn.relu(down4)
    #down4 = tf.nn.max_pool(down4, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    up1 = tflib.ops.deconv2d.Deconv2D(name+'_layer.5', 8*dim, 4*dim, 5, down4)
    up1 = tf.nn.relu(up1)

    up1 = custom_concat(1, [down3, up1]) #skip connection
    up2 = tflib.ops.deconv2d.Deconv2D(name+'._layer.6', 2*4*dim, 2*dim, 5, up1)
    up2 = tf.nn.relu(up2)

    up2 = custom_concat(1, [down2, up2]) #skip connection
    up3 = tflib.ops.deconv2d.Deconv2D(name+'._layer.7', 2*2*dim, dim, 5, up2)
    up3 = tf.nn.relu(up3)

    up3 = custom_concat(1, [down1, up3]) #skip connection
    up4 = tflib.ops.deconv2d.Deconv2D(name+'.layer.8', 2*dim, 1, 5, up3)
    up4 = tf.nn.sigmoid(up4)

    return tf.reshape(up4, [-1, 1*configs.output_dim])

def cGenerator5(inputs, name, dim=32, channel_size=2):
    output = tf.reshape(inputs, [-1, channel_size, DATA_LEN, DATA_LEN])

    down1 = tflib.ops.conv2d.Conv2D(name+'_layer.1', channel_size, dim, 5, output, stride=1) #down1 64*64
    down1 = tf.nn.relu(down1)
    down2 = tf.nn.max_pool(down1, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    down2 = tflib.ops.conv2d.Conv2D(name+'_layer.2', dim, 2*dim, 5, down2, stride=1) #down2 32*32
    down2 = tf.nn.relu(down2)
    down3 = tf.nn.max_pool(down2, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW') 

    down3 = tflib.ops.conv2d.Conv2D(name+'_layer.3', 2*dim, 4*dim, 5, down3, stride=1) #down3 16*16
    down3 = tf.nn.relu(down3)
    down4 = tf.nn.max_pool(down3, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW')

    down4 = tflib.ops.conv2d.Conv2D(name+'_layer.4', 4*dim, 8*dim, 5, down4, stride=1) #down4 8*8
    down4 = tf.nn.relu(down4)
    down5 = tf.nn.max_pool(down4, [1, 1, 2, 2], [1, 1, 2, 2], 'SAME', 'NCHW') #down5 4*4


    up1 = tflib.ops.deconv2d.Deconv2D(name+'_layer.5', 8*dim, 4*dim, 5, down5) #up1 8*8
    up1 = tf.nn.relu(up1)

    up1 = custom_concat(1, [down4, up1]) #skip connection
    up2 = tflib.ops.deconv2d.Deconv2D(name+'._layer.6', 8*dim+4*dim, 2*dim, 5, up1) #up2 16*16
    up2 = tf.nn.relu(up2)

    up2 = custom_concat(1, [down3, up2]) #skip connection
    up3 = tflib.ops.deconv2d.Deconv2D(name+'._layer.7', 4*dim+2*dim, dim, 5, up2) #up3 32*32
    up3 = tf.nn.relu(up3)

    up3 = custom_concat(1, [down2, up3]) #skip connection
    up4 = tflib.ops.deconv2d.Deconv2D(name+'.layer.8', 2*dim + dim, dim, 5, up3) #up4 64*64

    up4 = custom_concat(1, [down1, up4])
    up5 = tflib.ops.conv2d.Conv2D(name+'.layer.9', 2*dim, 1, 5, up4, stride = 1) #up4 64*64

    up5 = tf.nn.sigmoid(up5)

    return tf.reshape(up5, [-1, 1*configs.output_dim])

def pGenerator32(name, noise, code_size, dim):
    output = tflib.ops.linear.Linear(name+'.L1', code_size, 4*4*4*dim, noise)
    output = tf.nn.relu(output)
    output = tf.reshape(output, [-1, 4*dim, 4, 4])

    output = tflib.ops.deconv2d.Deconv2D(name+'.L2', 4*dim, 2*dim, 5, output)
    output = tf.nn.relu(output)
    output = output[:,:,:7,:7]

    output = tflib.ops.deconv2d.Deconv2D(name+'.L3', 2*dim, dim, 5, output)
    output = tf.nn.relu(output)
    #output = tflib.ops.deconv2d.Deconv2D('Generator.3', 2*DIM, 1, 5, output, strides=[1,4,4,1])

    output = tflib.ops.deconv2d.Deconv2D(name+'.L4', dim, 1, 5, output)
    output = tf.nn.sigmoid(output)

    return tf.reshape(output, [-1, configs.output_dim])

def pGenerator64(name, noise, code_size, dim):
    output = tflib.ops.linear.Linear(name+'.L1', code_size, 8*4*4*dim, noise)
    output = tf.nn.relu(output)
    output = tf.reshape(output, [-1, 8*dim, 4, 4])

    output = tflib.ops.deconv2d.Deconv2D(name+'.L2', 8*dim, 4*dim, 5, output)
    output = tf.nn.relu(output)

    output = tflib.ops.deconv2d.Deconv2D(name+'.L3', 4*dim, 2*dim, 5, output)
    output = tf.nn.relu(output)

    output = tflib.ops.deconv2d.Deconv2D(name+'.L4', 2*dim, dim, 5, output)
    output = tf.nn.relu(output)

    output = tflib.ops.deconv2d.Deconv2D(name+'.L5', dim, 1, 5, output)
    output = tf.nn.sigmoid(output)
   
    return tf.reshape(output, [-1, configs.output_dim])

def compute_grad_penalty(fake_samples, real_samples, target_disc_name, disc_dim, disc_ch_num):
    # Gradient penalty samples
    alpha = tf.random_uniform(
        shape=[hparams.batch_size, 1], 
        minval=0.,
        maxval=1.
    )
    differences = fake_samples - real_samples
    interpolates = real_samples + (alpha*differences)
    inter_vals = Discriminator(interpolates, target_disc_name, disc_dim, disc_ch_num)
    gradients = tf.gradients(inter_vals, interpolates)[0]
    slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), reduction_indices=[1]))
    gradient_penalty = tf.reduce_mean(tf.maximum(0.,(slopes-1.))**2)
    return gradient_penalty

if configs.dataset == 'mnist-background' or configs.dataset == 'fashion-mnist-background':
    Discriminator = Discriminator32
    dGenerator = d2Generator32
    cGenerator = cGenerator1
    pGenerator = pGenerator32
    fg_code_len = fg_hparams.code_size
    bg_code_len = bg_hparams.code_size
    fg_dim = fg_hparams.gen_dim
    bg_dim = bg_hparams.gen_dim

elif configs.dataset == 'mnist-background-box1':
    Discriminator = Discriminator64
    cGenerator = cGenerator2
elif configs.dataset == 'mnist-background-box2' or configs.dataset == 'mnist-background-box3' or configs.dataset == 'mnist-background-box3-noscale':
    Discriminator = Discriminator64
    cGenerator = cGenerator4
    dGenerator = d2Generator64
    pGenerator = pGenerator64
    fg_code_len = fg_hparams.code_size
    bg_code_len = bg_hparams.code_size
    fg_dim = fg_hparams.gen_dim
    bg_dim = bg_hparams.gen_dim

# generator for foreground or background
gen_name_f = 'mnist-f_generator'
gen_name_b = 'mnist-b_generator'
#matched the spec of pretrained models
if configs.label_used == -1:
    real_comp, _, real_bg, real_fg = MnistB.next_elements['train']
else:
    real_comp, _, real_bg, real_fg = MnistB.next_elements['train_{}'.format(configs.label_used)]

real_data_fg = real_fg
real_data_bg = real_bg

if not configs.real_fg:
    real_fg = pGenerator(gen_name_f, tf.random_normal([fg_hparams.batch_size, fg_code_len]), fg_code_len, fg_dim)
    fixed_fg_noise_ph = tf.placeholder(tf.float32, [fg_hparams.batch_size, fg_code_len])
    fixed_fg = pGenerator(gen_name_f, fixed_fg_noise_ph, fg_code_len, fg_dim)
if not configs.real_bg:
    real_bg = pGenerator(gen_name_b, tf.random_normal([bg_hparams.batch_size, bg_code_len]), bg_code_len, bg_dim)
    fixed_bg_noise_ph = tf.placeholder(tf.float32, [bg_hparams.batch_size, bg_code_len])
    fixed_bg = pGenerator(gen_name_b, fixed_bg_noise_ph, bg_code_len, bg_dim)

if configs.fg_model_path:
    saver_f = tf.train.Saver(tflib.params_with_name(gen_name_f))
if configs.bg_model_path:
    saver_b = tf.train.Saver(tflib.params_with_name(gen_name_b)) 

#construct decomposition graph and cost
fake_bg_fg = dGenerator(real_comp, 'DecompositeGenerator_bg_fg', hparams.decomp_gen_dim, 1)
fixed_bg_fg_ph = tf.placeholder(tf.float32, [configs.v_size, configs.output_dim])
fixed_fake_bg_fg = dGenerator(fixed_bg_fg_ph, 'DecompositeGenerator_bg_fg', hparams.decomp_gen_dim, 1)
if configs.decomp_use_side_info == False:
    real_bg_fg = tf.reshape(custom_concat(1, [tf.reshape(real_bg, [-1, 1, configs.output_dim]), 
                                              tf.reshape(real_fg, [-1, 1, configs.output_dim])]), 
                            [-1, 2*configs.output_dim])
    disc_real_bg_fg = Discriminator(real_bg_fg, 'DecompositieDiscriminator_bg_fg', hparams.decomp_disc_dim, 2)
    disc_fake_bg_fg = Discriminator(fake_bg_fg, 'DecompositieDiscriminator_bg_fg', hparams.decomp_disc_dim, 2)
    gp_bg_fg = compute_grad_penalty(fake_bg_fg, real_bg_fg, 'DecompositieDiscriminator_bg_fg', hparams.decomp_disc_dim, 2)
else:
    real_bg_fg_comp = tf.reshape( custom_concat(1, [tf.reshape(real_bg, [-1, 1, configs.output_dim]),
                                                    tf.reshape(real_fg, [-1, 1, configs.output_dim]),
                                                    tf.reshape(real_comp, [-1, 1, configs.output_dim])]),
                               [-1, 3*configs.output_dim] )
    fake_bg_fg_comp = tf.reshape( custom_concat(1, [tf.reshape(fake_bg_fg, [-1, 2, configs.output_dim]),
                                                   tf.reshape(real_comp, [-1, 1, configs.output_dim])]),
                               [-1, 3*configs.output_dim] )
    disc_real_bg_fg = Discriminator(real_bg_fg_comp, 'DecompositieDiscriminator_bg_fg', hparams.decomp_disc_dim, 3)
    disc_fake_bg_fg = Discriminator(fake_bg_fg_comp, 'DecompositieDiscriminator_bg_fg', hparams.decomp_disc_dim, 3)
    gp_bg_fg = compute_grad_penalty(fake_bg_fg_comp, real_bg_fg_comp, 'DecompositieDiscriminator_bg_fg', hparams.decomp_disc_dim, 3)
disc_cost_decomp = tf.reduce_mean(disc_fake_bg_fg) - tf.reduce_mean(disc_real_bg_fg) 
if configs.decomp_use_side_info:
    f_bg = fake_bg_fg[:, :configs.output_dim]
    f_fg = fake_bg_fg[:, configs.output_dim:]
    gen_cost_decomp =  tf.reduce_mean(tf.reduce_sum(tf.abs(f_fg - real_fg), 1)) + \
                       tf.reduce_mean(tf.reduce_sum(tf.abs(f_bg - real_bg), 1))
else:
    gen_cost_decomp = -tf.reduce_mean(disc_fake_bg_fg)
wdist_decomp = -disc_cost_decomp
disc_cost_decomp += hparams.LAMBDA*gp_bg_fg
gp_decomp = gp_bg_fg
gen_decomp_params = tflib.params_with_name('DecompositeGenerator')
disc_decomp_params = tflib.params_with_name('DecompositieDiscriminator')

#construct composition graph and cost
real_bg_fg = custom_concat(1, [tf.reshape(real_bg, [-1, 1, configs.output_dim]), 
                               tf.reshape(real_fg, [-1, 1, configs.output_dim])] )
fake_comp = cGenerator(real_bg_fg, 'CompositeGenerator', hparams.comp_gen_dim, 2)
fixed_comp_ph = tf.placeholder(tf.float32, [configs.v_size, 2, configs.output_dim])
fixed_fake_comp = cGenerator(fixed_comp_ph, 'CompositeGenerator', hparams.comp_gen_dim, 2)
if configs.comp_use_side_info == False:
    disc_real_comp = Discriminator(real_comp, 'CompositieDiscriminator', hparams.comp_disc_dim, 1)
    disc_fake_comp = Discriminator(fake_comp, 'CompositieDiscriminator', hparams.comp_disc_dim, 1)
    gp_comp = compute_grad_penalty(fake_comp, real_comp, 'CompositieDiscriminator', hparams.comp_disc_dim, 1)
else:
    real_bg_fg_comp = tf.reshape( custom_concat(1, [real_bg_fg, 
                                   tf.reshape(real_comp, [-1, 1, configs.output_dim])]),
                                  [-1, 3*configs.output_dim] )
    fake_bg_fg_comp = tf.reshape( custom_concat(1, [real_bg_fg, 
                                   tf.reshape(fake_comp, [-1, 1, configs.output_dim])]),
                                  [-1, 3*configs.output_dim] )
    disc_real_comp = Discriminator(real_bg_fg_comp, 'CompositieDiscriminator', hparams.comp_disc_dim, 3)
    disc_fake_comp = Discriminator(fake_bg_fg_comp, 'CompositieDiscriminator', hparams.comp_disc_dim, 3)
    gp_comp = compute_grad_penalty(fake_bg_fg_comp, real_bg_fg_comp, 'CompositieDiscriminator', hparams.comp_disc_dim, 3)
gen_comp_params = tflib.params_with_name('CompositeGenerator')
disc_comp_params = tflib.params_with_name('CompositieDiscriminator')
if configs.comp_use_side_info:
    gen_cost_comp = tf.reduce_mean(tf.reduce_sum(tf.abs(fake_comp-real_comp), 1))
else:
    gen_cost_comp = -tf.reduce_mean(disc_fake_comp)
disc_cost_comp = tf.reduce_mean(disc_fake_comp) - tf.reduce_mean(disc_real_comp) 
wdist_comp = -disc_cost_comp
disc_cost_comp += hparams.LAMBDA*gp_comp

# construct all costs
gen_cost_all = 0
disc_cost_all = 0
wdist_all = 0
gp_all = 0
gen_all_params = []
disc_all_params = []
if (configs.train_comp and configs.train_decomp) or (configs.train_bg or configs.train_fg):
    gen_cost_all += (gen_cost_decomp + gen_cost_comp)
    disc_cost_all += (disc_cost_decomp + disc_cost_comp)
    wdist_all += (wdist_decomp+wdist_comp)
    gp_all += (gp_decomp+gp_comp)
    disc_all_params.extend(disc_comp_params)
    disc_all_params.extend(disc_decomp_params)
    if configs.train_comp:
        gen_all_params.extend(gen_comp_params)
    if configs.train_decomp:
        gen_all_params.extend(gen_decomp_params)
    if configs.train_bg:
        gen_all_params.extend(tflib.params_with_name(gen_name_b))
    if configs.train_fg:
        gen_all_params.extend(tflib.params_with_name(gen_name_f))
elif configs.train_comp:
    print('train comp only')
    gen_cost_all += gen_cost_comp
    disc_cost_all += disc_cost_comp
    wdist_all += wdist_comp
    gp_all += gp_comp
    gen_all_params.extend(gen_comp_params)
    disc_all_params.extend(disc_comp_params)
elif configs.train_decomp:
    print('train decomp only')
    gen_cost_all += gen_cost_decomp
    disc_cost_all += disc_cost_decomp
    wdist_all += wdist_decomp
    gp_all += gp_decomp
    gen_all_params.extend(gen_decomp_params)
    disc_all_params.extend(disc_decomp_params) 

# cycle gan constraints
if hparams.cycle_loss_p > 0:
    # fake_bg_fg = custom_concat(1, [tf.reshape(fake_bg, [-1, 1, configs.output_dim]), 
    #                                tf.reshape(fake_fg, [-1, 1, configs.output_dim])])
    rerec_comp = cGenerator(fake_bg_fg, 'CompositeGenerator', hparams.comp_gen_dim, 2)
    #decomp_cycle_loss = tf.reduce_mean( tf.reduce_sum( (real_comp - rerec_comp)**2, [1] ) )
    decomp_cycle_loss = tf.reduce_mean( tf.reduce_sum( tf.abs(real_comp - rerec_comp), [1] ) )
    rerec_bg_fg = dGenerator(fake_comp, 'DecompositeGenerator_bg_fg', hparams.decomp_gen_dim, 1)
    rerec_bg_fg = tf.reshape(rerec_bg_fg, [-1, 2, configs.output_dim])
    rerec_bg = tf.reshape(rerec_bg_fg[:, 0, :], [-1, configs.output_dim])
    rerec_fg = tf.reshape(rerec_bg_fg[:, 1, :], [-1, configs.output_dim])
    #comp_cycle_loss = .5*tf.reduce_mean( tf.reduce_sum( (real_bg - rerec_bg)**2, [1] ) ) + \
    #                  .5*tf.reduce_mean( tf.reduce_sum( (real_fg - rerec_fg)**2, [1] ) )
    comp_cycle_loss = .5*tf.reduce_mean( tf.reduce_sum( tf.abs(real_bg - rerec_bg), [1] ) ) + \
                      .5*tf.reduce_mean( tf.reduce_sum( tf.abs(real_fg - rerec_fg), [1] ) )
    #gen_cost_all += hparams.cycle_loss_p*(decomp_cycle_loss+comp_cycle_loss)
    cycle_weight_mul = tf.placeholder(tf.float32)
    gen_cost_all += cycle_weight_mul*(decomp_cycle_loss+comp_cycle_loss)

if not args.sampling:
    if not configs.comp_use_side_info:
        gen_all_train_op = tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0.5, beta2=0.9).minimize(gen_cost_all, var_list=gen_all_params)
    else:
        gen_all_train_op = tf.train.AdamOptimizer().minimize(gen_cost_all, var_list=gen_all_params)
        #gen_all_train_op = tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0.5, beta2=0.9).minimize(gen_cost_all, var_list=gen_all_params)

    if not configs.decomp_use_side_info:
        disc_all_train_op = tf.train.AdamOptimizer(learning_rate=5e-4, beta1=0.5, beta2=0.9).minimize(disc_cost_all, var_list=disc_all_params)
    else:
        disc_all_train_op = tf.train.AdamOptimizer().minimize(disc_cost_all, var_list=disc_all_params)


def generate_image(frame, fixed_bimgs=None, fixed_fimgs=None):
# fixed_rec_bg = session.run(fixed_fake_bg, feed_dict={fixed_bg_ph: fixed_cimgs})
# fixed_rec_fg = session.run(fixed_fake_fg, feed_dict={fixed_fg_ph: fixed_cimgs})
    _fixed_fake_bg_fg = session.run(fixed_fake_bg_fg, feed_dict={fixed_bg_fg_ph: fixed_cimgs})
    _fixed_fake_bg_fg = _fixed_fake_bg_fg.reshape(-1, 2, DATA_LEN, DATA_LEN)
    fixed_rec_bg = _fixed_fake_bg_fg[:, 0, :, :]
    fixed_rec_fg = _fixed_fake_bg_fg[:, 1, :, :]
    if not configs.real_fg: 
        _fixed_fg = session.run(fixed_fg, feed_dict={fixed_fg_noise_ph: fixed_fg_noise})
        fixed_fimgs = _fixed_fg
        #fixed outputs from the fg model
        if frame == 0 or configs.train_fg:
            tflib.save_images.save_images(
            _fixed_fg.reshape((100, DATA_LEN, DATA_LEN)), 
            os.path.join( out_basedir, 'fg_{}.png'.format(frame+1)), 'fg, it={}'.format(frame+1))
    if not configs.real_bg:
        _fixed_bg = session.run(fixed_bg, feed_dict={fixed_bg_noise_ph: fixed_bg_noise})
        fixed_bimgs = _fixed_bg
        #fixed outputs from the bg model
        if frame == 0 or configs.train_bg:
            tflib.save_images.save_images(
            _fixed_bg.reshape((100, DATA_LEN, DATA_LEN)), 
            os.path.join( out_basedir, 'bg_{}.png'.format(frame+1)), 'bg, it={}'.format(frame+1))

    fixed_two_imgs = np.concatenate([fixed_bimgs.reshape(configs.v_size, 1, configs.output_dim), 
                                     fixed_fimgs.reshape(configs.v_size, 1, configs.output_dim)], axis=1)
   
    fixed_rec_c = session.run(fixed_fake_comp, feed_dict={fixed_comp_ph: fixed_two_imgs})

    fixed_rerec_bgfg = session.run(fixed_fake_bg_fg, feed_dict={fixed_bg_fg_ph: fixed_rec_c})
    #fixed decomposition outputs (decomposite of the image from real sample)
    if frame == 0 or configs.train_decomp or configs.train_fg or configs.train_bg:
        tflib.save_images.save_images(
            fixed_rec_bg.reshape((configs.v_size, DATA_LEN, DATA_LEN)), 
            os.path.join( out_basedir, 'recons_bg_{}.png'.format(frame+1)), 'reconstructed bg, it={}'.format(frame+1))
        tflib.save_images.save_images(
            fixed_rec_fg.reshape((configs.v_size, DATA_LEN, DATA_LEN)), 
            os.path.join( out_basedir, 'recons_fg_{}.png'.format(frame+1)), 'reconstructed fg, it={}'.format(frame+1))
    #fixed composition outputs (composite of images from real sample if configs.real_fg and configs.real_bg or fake samples from fg and bg generators)
    if frame == 0 or configs.train_comp or configs.train_fg or configs.train_bg:
        tflib.save_images.save_images(
        fixed_rec_c.reshape((configs.v_size, DATA_LEN, DATA_LEN)), 
        os.path.join( out_basedir, 'recons_c_{}.png'.format(frame+1)), 'reconstructed comp, it={}'.format(frame+1))

    if hparams.cycle_loss_p > 0:
        fixed_rerec_c = session.run(fixed_fake_comp, feed_dict={fixed_comp_ph: _fixed_fake_bg_fg.reshape(-1, 2, configs.output_dim)})
        #outputs of comp_model(decomp_model(real_sample))
        tflib.save_images.save_images(
        fixed_rerec_c.reshape((configs.v_size, DATA_LEN, DATA_LEN)), 
        os.path.join( out_basedir, 'recons_recc_{}.png'.format(frame+1)), 'reconstructed re-comp, it={}'.format(frame+1))
        #outputs of decomp_model(comp_model([fg, bg]))
        tflib.save_images.save_images(
        fixed_rerec_bgfg.reshape(-1, 2, configs.output_dim)[:, 0, :].reshape((configs.v_size, DATA_LEN, DATA_LEN)), 
        os.path.join( out_basedir, 'recons_recbg_{}.png'.format(frame+1)), 'reconstructed re-bg, it={}'.format(frame+1))
        tflib.save_images.save_images(
        fixed_rerec_bgfg.reshape(-1, 2, configs.output_dim)[:, 1, :].reshape((configs.v_size, DATA_LEN, DATA_LEN)), 
        os.path.join( out_basedir, 'recons_recfg_{}.png'.format(frame+1)), 'reconstructed re-fg, it={}'.format(frame+1))

    # if hparams.cycle_loss_p > 0:
    #     _fixed_rec_comp = session.run(fixed_rec_comp, feed_dict={fixed_bg_ph: fixed_cimgs, fixed_fg_ph: fixed_cimgs})
    #     tflib.save_images.save_images(
    #     _fixed_rec_comp.reshape((configs.v_size, DATA_LEN, DATA_LEN)), 
    #     os.path.join( out_basedir, 're-recons_c_{}.png'.format(frame+1)), 're-reconstructed comp, it={}'.format(frame+1))

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=.4)
savers = {}
comp_params = gen_comp_params[:] #deep copy
#comp_params.extend(disc_comp_params)
savers['comp_gen'] = ( tf.train.Saver(comp_params), os.path.join( out_basedir, 'comp_gen' ) )
decomp_params = gen_decomp_params[:]
#decomp_params.extend(disc_decomp_params)
savers['decomp_gen'] = ( tf.train.Saver(decomp_params), os.path.join( out_basedir, 'decomp_gen') )
if configs.train_bg:
    savers['bg'] = ( tf.train.Saver(tflib.params_with_name(gen_name_b)), os.path.join( out_basedir, 'bg' ) )
if configs.train_fg:
    savers['fg'] = ( tf.train.Saver(tflib.params_with_name(gen_name_f)), os.path.join( out_basedir, 'fg') )



with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as session:
    if configs.label_used == -1:
        MnistB.init_iterator(session, 'train')
    else:
        MnistB.init_iterator(session, 'train_{}'.format(configs.label_used))
    #samples from the real data
    fixed_cimgs, fixed_bimgs, fixed_fimgs = session.run([real_comp, real_data_bg, real_data_fg])
    #get some sample images from the dataset
    if not args.sampling:
        tflib.save_images.save_images(
            fixed_cimgs.reshape((configs.v_size, DATA_LEN, DATA_LEN)), 
            os.path.join( out_basedir, 'samples_c.png'))
        tflib.save_images.save_images(
            fixed_bimgs.reshape((configs.v_size, DATA_LEN, DATA_LEN)), 
            os.path.join( out_basedir, 'samples_b.png'))
        tflib.save_images.save_images(
            fixed_fimgs.reshape((configs.v_size, DATA_LEN, DATA_LEN)), 
            os.path.join( out_basedir, 'samples_f.png'))
    np.random.seed(5678)
    fixed_fg_noise =  np.random.normal(size=[100, fg_code_len])
    fixed_bg_noise =  np.random.normal(size=[100, bg_code_len])

    if configs.comp_model_path:
        load_model(configs.comp_model_path, savers['comp_gen'][0])
    if configs.decomp_model_path:
        load_model(configs.decomp_model_path, savers['decomp_gen'][0])
    if configs.fg_model_path:
        load_model(configs.fg_model_path, saver_f)
    if configs.bg_model_path:
        load_model(configs.bg_model_path, saver_b)

    init_op = tf.variables_initializer(
            [v for v in tf.global_variables() if v.name.split(':')[0] in set(session.run(tf.report_uninitialized_variables()))])
    session.run(init_op)

    if args.sampling:
        for z in xrange(sampling_configs.sampling_rounds):
            tmp = []
            for i in xrange(sampling_configs.sampling_times):                                                                             
                tmp.append( (session.run(fake_comp)*256).astype('uint8') )
            gen_imgs = np.concatenate(tmp)
            gen_imgs = np.reshape(gen_imgs, [-1, 28, 28, 1]) 
            gen_imgs = np.tile(gen_imgs, [1, 1, 1, 3]) 
            with open(os.path.join( out_basedir, '{}_{}.npy'.format(sampling_configs.sampling_outfile_name, z) ), 'w') as f:
                np.save(f, gen_imgs)
    else:
        #collect values we want to view and training op
        gen_ops = []
        gen_name_dict = {}
        cnt = 1
        gen_ops.append(gen_all_train_op)
        if (configs.train_comp and configs.train_decomp) or (configs.train_fg or configs.train_bg):
            gen_ops.extend([gen_cost_decomp, gen_cost_comp])
            gen_name_dict[cnt] = 'train gen cost decomp'
            gen_name_dict[cnt+1] = 'train gen cost comp'
            cnt+=2
        elif configs.train_comp:
            gen_ops.append(gen_cost_comp)
            gen_name_dict[cnt] = 'train gen cost comp'
            cnt+=1
        elif configs.train_decomp:
            gen_ops.append(gen_cost_decomp)
            gen_name_dict[cnt] = 'train gen cost decomp'
            cnt+=1
        if hparams.cycle_loss_p > 0:
            gen_ops.extend([comp_cycle_loss, decomp_cycle_loss])
            gen_name_dict[cnt] = 'train comp cycle loss'
            gen_name_dict[cnt+1] = 'train decomp cycle loss'
        disc_ops = []
        disc_name_dict = {}
        cnt = 1
        disc_ops.append( disc_all_train_op )
        if (configs.train_comp and configs.train_decomp) or (configs.train_fg or configs.train_bg):
            disc_ops.extend([disc_cost_decomp, disc_cost_comp, 
                             wdist_decomp, wdist_comp, 
                             gp_decomp, gp_comp])
            disc_name_dict[cnt] = 'train disc cost decomp'
            disc_name_dict[cnt+1] = 'train disc cost comp'
            disc_name_dict[cnt+2] = 'train w-dist decomp'
            disc_name_dict[cnt+3] = 'train w-dist comp'
            disc_name_dict[cnt+4] = 'train gp decomp'
            disc_name_dict[cnt+5] = 'train gp comp'
        elif configs.train_comp:
            disc_ops.extend([disc_cost_comp, wdist_comp, gp_comp])
            disc_name_dict[cnt] = 'train disc cost comp'
            disc_name_dict[cnt+1] = 'train w-dist comp'
            disc_name_dict[cnt+2] = 'train gp comp'
        elif configs.train_decomp:
            disc_ops.extend([disc_cost_decomp, wdist_decomp, gp_decomp])
            disc_name_dict[cnt] = 'train disc cost decomp'
            disc_name_dict[cnt+1] = 'train w-dist decomp'
            disc_name_dict[cnt+2] = 'train gp decomp'
        #plot from model without training
        print(fixed_bimgs.shape)
        generate_image(0, fixed_bimgs, fixed_fimgs)
        avg_wdist_comp_vecs = np.zeros((100))
        avg_wdist_decomp_vecs = np.zeros((100)) 
        start = timer()
        for iteration in xrange(hparams.iters):
            if iteration > 0:
                if hparams.cycle_loss_p > 0:
                    g_logs = session.run(gen_ops, feed_dict={cycle_weight_mul: hparams.cycle_loss_p*(int(iteration/10000)+1)})
                else:
                    g_logs = session.run(gen_ops)
            for i in xrange(hparams.disc_iters):
                d_logs = session.run(disc_ops)
            for k in disc_name_dict.keys():
                if disc_name_dict[k] == 'train w-dist comp':
                    avg_wdist_comp_vecs[iteration % 100] = d_logs[k]
                if disc_name_dict[k] == 'train w-dist decomp':
                    avg_wdist_decomp_vecs[iteration % 100] = d_logs[k]
            if iteration % 5000 == 4999:
                cwd = os.getcwd()
                if configs.train_comp:
                    save_model(savers['comp_gen'][1], savers['comp_gen'][0])
                if configs.train_decomp:
                    save_model(savers['decomp_gen'][1], savers['decomp_gen'][0])
                if configs.train_fg:
                   save_model(savers['fg'][1], savers['fg'][0])
                if configs.train_bg:
                   save_model(savers['bg'][1], savers['bg'][0])

            # Calculate dev (test) loss  every 5000 iteration
            # if iteration % 5000 == 4999:
            #     test_disc_costs = []
            #     for images in test_gen():
            #         c_g_var = generate_g_var(BATCH_SIZE)       
            #         logs = session.run(
            #             [disc_cost],
            #             feed_dict={real_data_input: images }
            #         )
            #         test_disc_costs.append(logs[0])
            #     tflib.plot.plot( os.path.join(out_basedir, 'test disc cost'), np.mean(test_disc_costs))
            #     test_dist[test_cnt] = np.mean(test_disc_costs)
            #     test_cnt+=1
            if iteration % 1000 == 999:    
                generate_image(iteration, fixed_bimgs, fixed_fimgs)
            #if iteration % 2000 == 1999:
            #    gradient_vis_wrapper(iteration, CODE_SIZE)
            # Write logs every 100 iters
            if iteration % 100 == 99:
                end = timer()
                text = '{} it ({:.2f} s): '.format(iteration+1, end-start)
                for k in gen_name_dict.keys():
                    if gen_name_dict[k] == 'train gen cost decomp':
                        text += 'train decomp gen cost = {:.2f}, '.format(g_logs[k])
                    elif gen_name_dict[k] == 'train decomp cycle loss':
                        text += 'train decomp cycle loss = {:.2f}, '.format(g_logs[k])
                    if gen_name_dict[k] == 'train gen cost comp':
                        text += 'train comp gen cost = {:.2f}, '.format(g_logs[k])
                    elif gen_name_dict[k] == 'train comp cycle loss':
                        text += 'train comp cycle loss = {:.2f}, '.format(g_logs[k])
                    elif gen_name_dict[k] == 'train gen cost all':
                        text += 'train all gen cost = {:.2f}, '.format(g_logs[k])
                for k in disc_name_dict.keys():
                    if disc_name_dict[k] == 'train w-dist decomp':
                        text += 'train decomp w-dist = {:.2f}, train avg decomp w-dist = {:.2f}, '.format(d_logs[k], np.mean(avg_wdist_decomp_vecs))
                    elif disc_name_dict[k] == 'train w-dist comp':
                        text += 'train comp w-dist = {:.2f}, train avg comp w-dist = {:.2f} '.format(d_logs[k], np.mean(avg_wdist_comp_vecs))
                print(text)
                start = end
            tflib.plot.tick()
 