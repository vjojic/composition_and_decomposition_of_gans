"""
Image grid saver, based on color_grid_vis from github.com/Newmu
"""

import numpy as np
#import scipy.misc
#from scipy.misc import imsave
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def save_images(X, save_path, title=None, margin=0):
    # [0, 1] -> [0,255]
    if isinstance(X.flatten()[0], np.floating):
        X = (255.99*X).astype('uint8')
    else:
        X = X.astype('uint8')
    n_samples = X.shape[0]
    rows = int(np.sqrt(n_samples))
    while n_samples % rows != 0:
        rows -= 1

    nh, nw = rows, n_samples/rows

    if X.ndim == 2:
        X = np.reshape(X, (X.shape[0], int(np.sqrt(X.shape[1])), int(np.sqrt(X.shape[1]))))

    if X.ndim == 4:
        # BCHW -> BHWC
        X = X.transpose(0,2,3,1)
        h, w = X[0].shape[:2]
        img = np.zeros((h * nh + (nh - 1)*margin, w * nw + (nw - 1)*margin, 3), dtype='uint8')
    elif X.ndim == 3:
        h, w = X[0].shape[:2]
        img = np.zeros((h * nh + (nh - 1)*margin, w * nw + (nw - 1)*margin), dtype='uint8')

    for n, x in enumerate(X):
        j = n/nw
        i = n%nw
        img[j * (h + margin):j * (h + margin) + h, i * (w + margin):i * (w + margin) + w] = x
        if n < len(X):
            img[j * (h + margin) + h:j * (h + margin) + h + margin, i * (w + margin):i * (w + margin) + w] = 255
            img[j * (h + margin):j * (h + margin) + h, i * (w + margin) + w:i * (w + margin) + w + margin] = 255

    f, ax = plt.subplots()
    if X.ndim == 3:
        ax.imshow(img, interpolation='none', cmap='gray')
    elif X.ndim == 4:
        ax.imshow(img)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    if title != None:
        ax.set_title(title)
    plt.savefig(save_path)
    plt.close()


def save_saliency(X, save_path, title=None, margin=1):

    n_samples = X.shape[0]
    rows = int(np.sqrt(n_samples))   
    nh, nw = rows, n_samples/rows

    h, w = X[0].shape[:2]
    img = np.zeros((h * nh + (nh - 1)*margin, w * nw + (nw - 1)*margin))

    max_v = np.max(X)

    for n, x in enumerate(X):
        j = n/nw
        i = n%nw
        img[j * (h + margin):j * (h + margin) + h, i * (w + margin):i * (w + margin) + w] = x
        img[j * (h + margin) + h:j * (h + margin) + h + margin, i * (w + margin):i * (w + margin) + w] = max_v
        img[j * (h + margin):j * (h + margin) + h, i * (w + margin) + w:i * (w + margin) + w + margin] = max_v

    f, ax = plt.subplots()
    plt.imshow(img, cmap='hot', interpolation='nearest')
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
    plt.colorbar()
    if title != None:
        ax.set_title(title)
    plt.savefig(save_path)
    plt.close()