import numpy as np
import os
import urllib
import gzip
import cPickle as pickle

import tensorflow as tf

import numpy as np

def random_shuffle_data(data, c_seed=1234):
    #rng_state = np.random.get_state()
    np.random.seed(c_seed)
    for d in data:
        np.random.shuffle(d)
        #np.random.set_state(rng_state)
        np.random.seed(c_seed)

def unpickle(file):
    fo = gzip.open(file, 'rb')
    images, labels = pickle.load(fo)
    fo.close()
    return images, labels

def read_all_data(filenames, data_dir):
    all_data = []
    all_labels = []
    for filename in filenames:
        tmp = unpickle(data_dir + '/' + filename)
        all_data.append(tmp[0])
        all_labels.append(tmp[1])
    images = np.concatenate(all_data, axis=0)
    labels = np.concatenate(all_labels, axis=0)
    return images, labels

class Mnist_datasets():
    def __init__(self, train_batch_size, test_batch_size, label_ind_dataset=False):
        filepath = '/tmp/mnist.pkl.gz'
        url = 'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'

        if not os.path.isfile(filepath):
            print "Couldn't find MNIST dataset in /tmp, downloading..."
            urllib.urlretrieve(url, filepath)

        with gzip.open('/tmp/mnist.pkl.gz', 'rb') as f:
            self.train_data, self.dev_data, self.test_data = pickle.load(f)
        random_shuffle_data(self.train_data)
        random_shuffle_data(self.dev_data)
        random_shuffle_data(self.test_data)

        data_ph = tf.placeholder(self.train_data[0].dtype, [None, 784], name='mnist_samples')
        label_ph = tf.placeholder(self.train_data[1].dtype, [None], name='mnist_labels')
        self.datasets = {}
        self.iterators = {}
        self.next_elements = {}

        self.datasets['train'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph))
        self.datasets['train'] = self.datasets['train'].batch(train_batch_size)
        self.datasets['train'] = self.datasets['train'].prefetch(1)
        self.datasets['train'] = self.datasets['train'].repeat()
        self.iterators['train'] = self.datasets['train'].make_initializable_iterator()
        self.next_elements['train'] = self.iterators['train'].get_next()

        self.datasets['dev'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph))
        self.datasets['dev'] = self.datasets['dev'].batch(test_batch_size)
        self.datasets['dev'] = self.datasets['dev'].prefetch(1)
        self.iterators['dev'] = self.datasets['dev'].make_initializable_iterator()
        self.next_elements['dev'] = self.iterators['dev'].get_next()

        self.datasets['test'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph))
        self.datasets['test'] = self.datasets['test'].batch(test_batch_size)
        self.datasets['test'] = self.datasets['test'].prefetch(1)
        self.iterators['test'] = self.datasets['test'].make_initializable_iterator() 
        self.next_elements['test'] = self.iterators['test'].get_next()

        if label_ind_dataset == True:
            self.train_data_dict = {}
            self.dev_data_dict = {}
            self.test_data_dict = {}
            for i in xrange(10):
                c_train = 'train_{}'.format(i)
                c_dev = 'dev_{}'.format(i)
                c_test = 'test_{}'.format(i)
                self.datasets[c_train] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph))
                self.datasets[c_train] = self.datasets[c_train].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_train] = self.datasets[c_train].prefetch(1)
                self.datasets[c_train] = self.datasets[c_train].repeat()
                self.iterators[c_train] = self.datasets[c_train].make_initializable_iterator()
                self.next_elements[c_train] = self.iterators[c_train].get_next()
                self.datasets[c_dev] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph))
                self.datasets[c_dev] = self.datasets[c_dev].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_dev] = self.datasets[c_dev].prefetch(1)
                self.datasets[c_dev] = self.datasets[c_dev].repeat()
                self.iterators[c_dev] = self.datasets[c_dev].make_initializable_iterator()   
                self.next_elements[c_dev] = self.iterators[c_dev].get_next()            
                self.datasets[c_test] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph))
                self.datasets[c_test] = self.datasets[c_test].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_test] = self.datasets[c_test].prefetch(1)
                self.datasets[c_test] = self.datasets[c_test].repeat()
                self.iterators[c_test] = self.datasets[c_test].make_initializable_iterator() 
                self.next_elements[c_test] = self.iterators[c_test].get_next()    

                self.train_data_dict[i] = self.train_data[0][self.train_data[1]==i]
                self.dev_data_dict[i] = self.dev_data[0][self.dev_data[1]==i]
                self.test_data_dict[i] = self.test_data[0][self.test_data[1]==i]


    def init_iterator(self, sess, data_set):
        if not data_set[-1].isdigit():
            if data_set == 'train':
                c_data = self.train_data
            elif data_set == 'dev':
                c_data = self.dev_data
            elif data_set == 'test':
                c_data = self.test_data
            sess.run(self.iterators[data_set].initializer, feed_dict={'mnist_samples:0': c_data[0],
                                          'mnist_labels:0': c_data[1]})
        else:
            c_lab = int(data_set[-1])
            if data_set.startswith('train'):
                c_data = self.train_data_dict[c_lab]
            elif data_set.startswith('dev'):
                c_data = self.dev_data_dict[c_lab]
            elif data_set.startswith('test'):
                c_data = self.test_data_dict[c_lab]
            c_lab = np.ones((c_data.shape[0]), dtype=self.train_data[1].dtype)*c_lab
            sess.run(self.iterators[data_set].initializer, feed_dict={'mnist_samples:0': c_data,
                                          'mnist_labels:0': c_lab})                

class MnistBackground_datasets():

    def __init__(self, train_batch_size, test_batch_size, noise_fg=False, label_ind_dataset=False, c_seed=1234):
        filepath = '/tmp/mnist_background.pkl.gz'
        url = 'http://www.cs.unc.edu/~ycharn/data/mnist_background.pkl.gz'

        if not os.path.isfile(filepath):
            print "Couldn't find MNIST-background dataset in /tmp, downloading..."
            urllib.urlretrieve(url, filepath)

        with gzip.open('/tmp/mnist_background.pkl.gz', 'rb') as f:
            self.train_data, self.dev_data, self.test_data = pickle.load(f)
        tmp = np.bincount(self.train_data[3])
        self.train_lab_dist = np.bincount(self.train_data[3])/float(self.train_data[3].shape[0])
        random_shuffle_data(self.train_data, c_seed)
        random_shuffle_data(self.dev_data, c_seed)
        random_shuffle_data(self.test_data, c_seed)

        if noise_fg == True:
            idx = np.where(self.train_data[2]<.1)
            tmp = np.abs(np.random.normal(0, .25, len(idx[0])))
            tmp[tmp>1] = 1
            self.train_data[2][idx] = tmp
            idx = np.where(self.dev_data[2]<.1)
            tmp = np.abs(np.random.normal(0, .25, len(idx[0])))
            tmp[tmp>1] = 1
            self.dev_data[2][idx] = tmp
            idx = np.where(self.test_data[2]<.1)
            tmp = np.abs(np.random.normal(0, .25, len(idx[0])))
            tmp[tmp>1] = 1
            self.test_data[2][idx] = tmp

        data_ph = tf.placeholder(self.train_data[0].dtype, [None, 784], name='mnistbackground_samples')
        label_ph = tf.placeholder(self.train_data[-1].dtype, [None], name='mnistbackground_labels')
        bdata_ph = tf.placeholder(self.train_data[0].dtype, [None, 784], name='mnistbackground_bsamples')
        fdata_ph = tf.placeholder(self.train_data[0].dtype, [None, 784], name='mnistbackground_fsamples')
        self.datasets = {}
        self.iterators = {}
        self.next_elements = {}

        self.datasets['train'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
        self.datasets['train'] = self.datasets['train'].batch(train_batch_size)
        self.datasets['train'] = self.datasets['train'].prefetch(2)
        self.datasets['train'] = self.datasets['train'].repeat()
        self.iterators['train'] = self.datasets['train'].make_initializable_iterator()
        self.next_elements['train'] = self.iterators['train'].get_next()

        self.datasets['dev'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
        self.datasets['dev'] = self.datasets['dev'].batch(test_batch_size)
        self.datasets['dev'] = self.datasets['dev'].prefetch(2)
        self.iterators['dev'] = self.datasets['dev'].make_initializable_iterator()
        self.next_elements['dev'] = self.iterators['dev'].get_next()

        self.datasets['test'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
        self.datasets['test'] = self.datasets['test'].batch(test_batch_size)
        self.datasets['test'] = self.datasets['test'].prefetch(2)
        self.iterators['test'] = self.datasets['test'].make_initializable_iterator()
        self.next_elements['test'] = self.iterators['test'].get_next()

        if label_ind_dataset == True:
            self.train_data_dict = {}
            self.dev_data_dict = {}
            self.test_data_dict = {}
            for i in xrange(10):
                c_train = 'train_{}'.format(i)
                c_dev = 'dev_{}'.format(i)
                c_test = 'test_{}'.format(i)
                self.datasets[c_train] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
                #self.datasets[c_train] = self.datasets[c_train].batch(train_batch_size)
                self.datasets[c_train] = self.datasets[c_train].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_train] = self.datasets[c_train].prefetch(1)
                self.datasets[c_train] = self.datasets[c_train].repeat()
                self.iterators[c_train] = self.datasets[c_train].make_initializable_iterator()
                self.next_elements[c_train] = self.iterators[c_train].get_next()
                self.datasets[c_dev] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
                #self.datasets[c_dev] = self.datasets[c_dev].batch(train_batch_size)
                self.datasets[c_dev] = self.datasets[c_dev].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_dev] = self.datasets[c_dev].prefetch(1)
                self.datasets[c_dev] = self.datasets[c_dev].repeat()
                self.iterators[c_dev] = self.datasets[c_dev].make_initializable_iterator()   
                self.next_elements[c_dev] = self.iterators[c_dev].get_next()            
                self.datasets[c_test] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
                #self.datasets[c_test] = self.datasets[c_test].batch(train_batch_size)
                self.datasets[c_test] = self.datasets[c_test].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_test] = self.datasets[c_test].prefetch(1)
                self.datasets[c_test] = self.datasets[c_test].repeat()
                self.iterators[c_test] = self.datasets[c_test].make_initializable_iterator() 
                self.next_elements[c_test] = self.iterators[c_test].get_next()    

                self.train_data_dict[i] = (self.train_data[0][self.train_data[3]==i], 
                                           self.train_data[1][self.train_data[3]==i],
                                           self.train_data[2][self.train_data[3]==i])
                self.dev_data_dict[i] = (self.dev_data[0][self.dev_data[3]==i],
                                         self.dev_data[1][self.dev_data[3]==i],
                                         self.dev_data[2][self.dev_data[3]==i])
                self.test_data_dict[i] = (self.test_data[0][self.test_data[3]==i],
                                          self.test_data[1][self.test_data[3]==i],
                                          self.test_data[2][self.test_data[3]==i])

    def init_iterator(self, sess, data_set):
        if not data_set[-1].isdigit():
            if data_set == 'train':
                c_data = self.train_data
            elif data_set == 'dev':
                c_data = self.dev_data
            elif data_set == 'test':
                c_data = self.test_data
            sess.run(self.iterators[data_set].initializer, feed_dict={'mnistbackground_samples:0': c_data[0],
                                          'mnistbackground_labels:0': c_data[-1], 
                                          'mnistbackground_bsamples:0': c_data[1],
                                          'mnistbackground_fsamples:0': c_data[2]})
        else:
            c_lab = int(data_set[-1])
            if data_set.startswith('train'):
                c_data = self.train_data_dict[c_lab]
            elif data_set.startswith('dev'):
                c_data = self.dev_data_dict[c_lab]
            elif data_set.startswith('test'):
                c_data = self.test_data_dict[c_lab]
            c_lab = np.ones((c_data[0].shape[0]), dtype=self.train_data[3].dtype)*c_lab
            sess.run(self.iterators[data_set].initializer, feed_dict={'mnistbackground_samples:0': c_data[0],
                                          'mnistbackground_labels:0': c_lab, 
                                          'mnistbackground_bsamples:0': c_data[1],
                                          'mnistbackground_fsamples:0': c_data[2]})

class FashionMnistBackground_datasets():

    def __init__(self, train_batch_size, test_batch_size, noise_fg=False, label_ind_dataset=False):
        filepath = '/tmp/fashion-mnist_background.pkl.gz'
        url = 'http://www.cs.unc.edu/~ycharn/data/fashion-mnist_background.pkl.gz'

        if not os.path.isfile(filepath):
            print "Couldn't find fashion-mnist_background dataset in /tmp, downloading..."
            urllib.urlretrieve(url, filepath)

        with gzip.open('/tmp/fashion-mnist_background.pkl.gz', 'rb') as f:
            self.train_data, self.dev_data, self.test_data = pickle.load(f)
        tmp = np.bincount(self.train_data[3])
        self.train_lab_dist = np.bincount(self.train_data[3])/float(self.train_data[3].shape[0])
        random_shuffle_data(self.train_data)
        random_shuffle_data(self.dev_data)
        random_shuffle_data(self.test_data)

        if noise_fg == True:
            idx = np.where(self.train_data[2]<.1)
            tmp = np.abs(np.random.normal(0, .25, len(idx[0])))
            tmp[tmp>1] = 1
            self.train_data[2][idx] = tmp
            idx = np.where(self.dev_data[2]<.1)
            tmp = np.abs(np.random.normal(0, .25, len(idx[0])))
            tmp[tmp>1] = 1
            self.dev_data[2][idx] = tmp
            idx = np.where(self.test_data[2]<.1)
            tmp = np.abs(np.random.normal(0, .25, len(idx[0])))
            tmp[tmp>1] = 1
            self.test_data[2][idx] = tmp

        data_ph = tf.placeholder(self.train_data[0].dtype, [None, 784], name='mnistbackground_samples')
        label_ph = tf.placeholder(self.train_data[-1].dtype, [None], name='mnistbackground_labels')
        bdata_ph = tf.placeholder(self.train_data[0].dtype, [None, 784], name='mnistbackground_bsamples')
        fdata_ph = tf.placeholder(self.train_data[0].dtype, [None, 784], name='mnistbackground_fsamples')
        self.datasets = {}
        self.iterators = {}
        self.next_elements = {}

        self.datasets['train'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
        self.datasets['train'] = self.datasets['train'].batch(train_batch_size)
        self.datasets['train'] = self.datasets['train'].prefetch(2)
        self.datasets['train'] = self.datasets['train'].repeat()
        self.iterators['train'] = self.datasets['train'].make_initializable_iterator()
        self.next_elements['train'] = self.iterators['train'].get_next()

        self.datasets['dev'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
        self.datasets['dev'] = self.datasets['dev'].batch(test_batch_size)
        self.datasets['dev'] = self.datasets['dev'].prefetch(2)
        self.iterators['dev'] = self.datasets['dev'].make_initializable_iterator()
        self.next_elements['dev'] = self.iterators['dev'].get_next()

        self.datasets['test'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
        self.datasets['test'] = self.datasets['test'].batch(test_batch_size)
        self.datasets['test'] = self.datasets['test'].prefetch(2)
        self.iterators['test'] = self.datasets['test'].make_initializable_iterator()
        self.next_elements['test'] = self.iterators['test'].get_next()

        if label_ind_dataset == True:
            self.train_data_dict = {}
            self.dev_data_dict = {}
            self.test_data_dict = {}
            for i in xrange(10):
                c_train = 'train_{}'.format(i)
                c_dev = 'dev_{}'.format(i)
                c_test = 'test_{}'.format(i)
                self.datasets[c_train] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
                #self.datasets[c_train] = self.datasets[c_train].batch(train_batch_size)
                self.datasets[c_train] = self.datasets[c_train].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_train] = self.datasets[c_train].prefetch(1)
                self.datasets[c_train] = self.datasets[c_train].repeat()
                self.iterators[c_train] = self.datasets[c_train].make_initializable_iterator()
                self.next_elements[c_train] = self.iterators[c_train].get_next()
                self.datasets[c_dev] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
                #self.datasets[c_dev] = self.datasets[c_dev].batch(train_batch_size)
                self.datasets[c_dev] = self.datasets[c_dev].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_dev] = self.datasets[c_dev].prefetch(1)
                self.datasets[c_dev] = self.datasets[c_dev].repeat()
                self.iterators[c_dev] = self.datasets[c_dev].make_initializable_iterator()   
                self.next_elements[c_dev] = self.iterators[c_dev].get_next()            
                self.datasets[c_test] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
                #self.datasets[c_test] = self.datasets[c_test].batch(train_batch_size)
                self.datasets[c_test] = self.datasets[c_test].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_test] = self.datasets[c_test].prefetch(1)
                self.datasets[c_test] = self.datasets[c_test].repeat()
                self.iterators[c_test] = self.datasets[c_test].make_initializable_iterator() 
                self.next_elements[c_test] = self.iterators[c_test].get_next()    

                self.train_data_dict[i] = (self.train_data[0][self.train_data[3]==i], 
                                           self.train_data[1][self.train_data[3]==i],
                                           self.train_data[2][self.train_data[3]==i])
                self.dev_data_dict[i] = (self.dev_data[0][self.dev_data[3]==i],
                                         self.dev_data[1][self.dev_data[3]==i],
                                         self.dev_data[2][self.dev_data[3]==i])
                self.test_data_dict[i] = (self.test_data[0][self.test_data[3]==i],
                                          self.test_data[1][self.test_data[3]==i],
                                          self.test_data[2][self.test_data[3]==i])

    def init_iterator(self, sess, data_set):
        if not data_set[-1].isdigit():
            if data_set == 'train':
                c_data = self.train_data
            elif data_set == 'dev':
                c_data = self.dev_data
            elif data_set == 'test':
                c_data = self.test_data
            sess.run(self.iterators[data_set].initializer, feed_dict={'mnistbackground_samples:0': c_data[0],
                                          'mnistbackground_labels:0': c_data[-1], 
                                          'mnistbackground_bsamples:0': c_data[1],
                                          'mnistbackground_fsamples:0': c_data[2]})
        else:
            c_lab = int(data_set[-1])
            if data_set.startswith('train'):
                c_data = self.train_data_dict[c_lab]
            elif data_set.startswith('dev'):
                c_data = self.dev_data_dict[c_lab]
            elif data_set.startswith('test'):
                c_data = self.test_data_dict[c_lab]
            c_lab = np.ones((c_data[0].shape[0]), dtype=self.train_data[3].dtype)*c_lab
            sess.run(self.iterators[data_set].initializer, feed_dict={'mnistbackground_samples:0': c_data[0],
                                          'mnistbackground_labels:0': c_lab, 
                                          'mnistbackground_bsamples:0': c_data[1],
                                          'mnistbackground_fsamples:0': c_data[2]})


class MnistBackgroundInv_datasets():

    def __init__(self, train_batch_size, test_batch_size, noise_fg=False, label_ind_dataset=False):
        filepath = '/tmp/mnist_background_inv.pkl.gz'
        url = 'http://www.cs.unc.edu/~ycharn/data/mnist_background_inv.pkl.gz'

        if not os.path.isfile(filepath):
            print "Couldn't find MNIST-background dataset in /tmp, downloading..."
            urllib.urlretrieve(url, filepath)

        with gzip.open(filepath, 'rb') as f:
            self.train_data, self.dev_data, self.test_data = pickle.load(f)
        tmp = np.bincount(self.train_data[3])
        self.train_lab_dist = np.bincount(self.train_data[3])/float(self.train_data[3].shape[0])
        random_shuffle_data(self.train_data)
        random_shuffle_data(self.dev_data)
        random_shuffle_data(self.test_data)

        if noise_fg == True:
            idx = np.where(self.train_data[2]<.1)
            tmp = np.abs(np.random.normal(0, .25, len(idx[0])))
            tmp[tmp>1] = 1
            self.train_data[2][idx] = tmp
            idx = np.where(self.dev_data[2]<.1)
            tmp = np.abs(np.random.normal(0, .25, len(idx[0])))
            tmp[tmp>1] = 1
            self.dev_data[2][idx] = tmp
            idx = np.where(self.test_data[2]<.1)
            tmp = np.abs(np.random.normal(0, .25, len(idx[0])))
            tmp[tmp>1] = 1
            self.test_data[2][idx] = tmp

        data_ph = tf.placeholder(tf.float32, [None, 784], name='mnistbackgroundinv_samples')
        label_ph = tf.placeholder(tf.float32, [None], name='mnistbackgroundinv_labels')
        bdata_ph = tf.placeholder(tf.float32, [None, 784], name='mnistbackgroundinv_bsamples')
        fdata_ph = tf.placeholder(tf.float32, [None, 784], name='mnistbackgroundinv_fsamples')
        self.datasets = {}
        self.iterators = {}
        self.next_elements = {}

        self.datasets['train'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
        map( lambda x, y, r, z: ( (x/255.), y, (r/255.), (z/255.) ) )  
        self.datasets['train'] = self.datasets['train'].batch(train_batch_size)
        self.datasets['train'] = self.datasets['train'].prefetch(2)
        self.datasets['train'] = self.datasets['train'].repeat()
        self.iterators['train'] = self.datasets['train'].make_initializable_iterator()
        self.next_elements['train'] = self.iterators['train'].get_next()

        self.datasets['dev'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
        map( lambda x, y, r, z: ( (x/255.), y, (r/255.), (z/255.) ) )  
        self.datasets['dev'] = self.datasets['dev'].batch(test_batch_size)
        self.datasets['dev'] = self.datasets['dev'].prefetch(2)
        self.iterators['dev'] = self.datasets['dev'].make_initializable_iterator()
        self.next_elements['dev'] = self.iterators['dev'].get_next()

        self.datasets['test'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
        map( lambda x, y, r, z: ( (x/255.), y, (r/255.), (z/255.) ) )  
        self.datasets['test'] = self.datasets['test'].batch(test_batch_size)
        self.datasets['test'] = self.datasets['test'].prefetch(2)
        self.iterators['test'] = self.datasets['test'].make_initializable_iterator()
        self.next_elements['test'] = self.iterators['test'].get_next()

        if label_ind_dataset == True:
            self.train_data_dict = {}
            self.dev_data_dict = {}
            self.test_data_dict = {}
            for i in xrange(10):
                c_train = 'train_{}'.format(i)
                c_dev = 'dev_{}'.format(i)
                c_test = 'test_{}'.format(i)
                self.datasets[c_train] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
                map( lambda x, y, r, z: ( (x/255.), y, (r/255.), (z/255.) ) )  
                self.datasets[c_train] = self.datasets[c_train].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_train] = self.datasets[c_train].prefetch(1)
                self.datasets[c_train] = self.datasets[c_train].repeat()
                self.iterators[c_train] = self.datasets[c_train].make_initializable_iterator()
                self.next_elements[c_train] = self.iterators[c_train].get_next()
                self.datasets[c_dev] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
                map( lambda x, y, r, z: ( (x/255.), y, (r/255.), (z/255.) ) )  
                self.datasets[c_dev] = self.datasets[c_dev].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_dev] = self.datasets[c_dev].prefetch(1)
                self.datasets[c_dev] = self.datasets[c_dev].repeat()
                self.iterators[c_dev] = self.datasets[c_dev].make_initializable_iterator()   
                self.next_elements[c_dev] = self.iterators[c_dev].get_next()            
                self.datasets[c_test] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
                map( lambda x, y, r, z: ( (x/255.), y, (r/255.), (z/255.) ) )  
                self.datasets[c_test] = self.datasets[c_test].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_test] = self.datasets[c_test].prefetch(1)
                self.datasets[c_test] = self.datasets[c_test].repeat()
                self.iterators[c_test] = self.datasets[c_test].make_initializable_iterator() 
                self.next_elements[c_test] = self.iterators[c_test].get_next()    

                self.train_data_dict[i] = (self.train_data[0][self.train_data[3]==i], 
                                           self.train_data[1][self.train_data[3]==i],
                                           self.train_data[2][self.train_data[3]==i])
                self.dev_data_dict[i] = (self.dev_data[0][self.dev_data[3]==i],
                                         self.dev_data[1][self.dev_data[3]==i],
                                         self.dev_data[2][self.dev_data[3]==i])
                self.test_data_dict[i] = (self.test_data[0][self.test_data[3]==i],
                                          self.test_data[1][self.test_data[3]==i],
                                          self.test_data[2][self.test_data[3]==i])

    def init_iterator(self, sess, data_set):
        if not data_set[-1].isdigit():
            if data_set == 'train':
                c_data = self.train_data
            elif data_set == 'dev':
                c_data = self.dev_data
            elif data_set == 'test':
                c_data = self.test_data
            sess.run(self.iterators[data_set].initializer, feed_dict={'mnistbackgroundinv_samples:0': c_data[0],
                                          'mnistbackgroundinv_labels:0': c_data[-1], 
                                          'mnistbackgroundinv_bsamples:0': c_data[1],
                                          'mnistbackgroundinv_fsamples:0': c_data[2]})
        else:
            c_lab = int(data_set[-1])
            if data_set.startswith('train'):
                c_data = self.train_data_dict[c_lab]
            elif data_set.startswith('dev'):
                c_data = self.dev_data_dict[c_lab]
            elif data_set.startswith('test'):
                c_data = self.test_data_dict[c_lab]
            c_lab = np.ones((c_data[0].shape[0]), dtype=self.train_data[3].dtype)*c_lab
            sess.run(self.iterators[data_set].initializer, feed_dict={'mnistbackgroundinv_samples:0': c_data[0],
                                          'mnistbackgroundinv_labels:0': c_lab, 
                                          'mnistbackgroundinv_bsamples:0': c_data[1],
                                          'mnistbackgroundinv_fsamples:0': c_data[2]})            

class MnistBackgroundBox1_datasets():

    def __init__(self, train_batch_size, test_batch_size):
        filepath = '/tmp/mnist_background_box1.pkl.gz'
        url = 'http://www.cs.unc.edu/~ycharn/data/mnist_background_box1.pkl.gz'

        if not os.path.isfile(filepath):
            print "Couldn't find MNIST-background-box1 dataset in /tmp, downloading..."
            urllib.urlretrieve(url, filepath)

        with gzip.open('/tmp/mnist_background_box1.pkl.gz', 'rb') as f:
            self.train_data, self.dev_data, self.test_data = pickle.load(f)
        tmp = np.bincount(self.train_data[3])
        self.train_lab_dist = np.bincount(self.train_data[3])/float(self.train_data[3].shape[0])
        random_shuffle_data(self.train_data)
        random_shuffle_data(self.dev_data)
        random_shuffle_data(self.test_data)

        data_ph = tf.placeholder(self.train_data[0].dtype, [None, 784], name='mnistbackground-box1_samples')
        label_ph = tf.placeholder(self.train_data[-1].dtype, [None], name='mnistground-box1_labels')
        bdata_ph = tf.placeholder(self.train_data[0].dtype, [None, 784], name='mnistbackground-box1_bsamples')
        fdata_ph = tf.placeholder(self.train_data[0].dtype, [None, 784], name='mnistbackground-box1_fsamples')
        self.datasets = {}
        self.iterators = {}

        self.datasets['train'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
        self.datasets['train'] = self.datasets['train'].batch(train_batch_size)
        self.datasets['train'] = self.datasets['train'].prefetch(2)
        self.datasets['train'] = self.datasets['train'].repeat()
        self.iterators['train'] = self.datasets['train'].make_initializable_iterator()

        self.datasets['dev'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
        self.datasets['dev'] = self.datasets['dev'].batch(test_batch_size)
        self.datasets['dev'] = self.datasets['dev'].prefetch(2)
        self.iterators['dev'] = self.datasets['dev'].make_initializable_iterator()

        self.datasets['test'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph))
        self.datasets['test'] = self.datasets['test'].batch(test_batch_size)
        self.datasets['test'] = self.datasets['test'].prefetch(2)
        self.iterators['test'] = self.datasets['test'].make_initializable_iterator()

        self.next_elements = {}
        self.next_elements['train'] = self.iterators['train'].get_next()
        self.next_elements['dev'] = self.iterators['dev'].get_next()
        self.next_elements['test'] = self.iterators['test'].get_next()

    def init_iterator(self, sess, data_set):
        if data_set == 'train':
            sess.run(self.iterators['train'].initializer, feed_dict={'mnistbackground-box1_samples:0': self.train_data[0],
                                          'mnistground-box1_labels:0': self.train_data[-1], 
                                          'mnistbackground-box1_bsamples:0': self.train_data[1],
                                          'mnistbackground-box1_fsamples:0': self.train_data[2]})
        elif data_set == 'dev':
            sess.run(self.iterators['dev'].initializer, feed_dict={'mnistbackground-box1_samples:0': self.dev_data[0],
                                          'mnistground-box1_labels:0': self.dev_data[-1],
                                          'mnistbackground-box1_bsamples:0': self.dev_data[1],
                                          'mnistbackground-box1_fsamples:0': self.dev_data[2]})
        elif data_set == 'test':
            sess.run(self.iterators['test'].initializer, feed_dict={'mnistbackground-box1_samples:0': self.test_data[0],
                                          'mnistground-box1_labels:0': self.test_data[-1],
                                          'mnistbackground-box1_bsamples:0': self.test_data[1],
                                          'mnistbackground-box1_fsamples:0': self.test_data[2]})

class MnistBackgroundBox2_datasets():

    def __init__(self, train_batch_size, test_batch_size):
        filepath = '/tmp/mnist_background_box2.pkl.gz'
        url = 'http://www.cs.unc.edu/~ycharn/data/mnist_background_box2.pkl.gz'

        if not os.path.isfile(filepath):
            print "Couldn't find MNIST-background-box2 dataset in /tmp, downloading..."
            urllib.urlretrieve(url, filepath)

        with gzip.open('/tmp/mnist_background_box2.pkl.gz', 'rb') as f:
            self.train_data, self.dev_data, self.test_data = pickle.load(f)
        tmp = np.bincount(self.train_data[3])
        self.train_lab_dist = np.bincount(self.train_data[3])/float(self.train_data[3].shape[0])
        random_shuffle_data(self.train_data)
        random_shuffle_data(self.dev_data)
        #random_shuffle_data(self.test_data)

        data_ph = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-box2_samples')
        label_ph = tf.placeholder(self.train_data[-1].dtype, [None], name='mnistground-box2_labels')
        bdata_ph = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-box2_bsamples')
        fdata_ph = tf.placeholder(tf.float32, [None, 784], name='mnistbackground-box2_fsamples')
        self.datasets = {}
        self.iterators = {}

        self.datasets['train'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
        map( lambda x, y, r, z: ( (x/255.), y, (r/255.), tf.squeeze(tf.reshape(tf.pad(tf.reshape(z, [-1, 28, 28])/255., [[0,0], [0, 36], [0, 36]]), [-1, 64*64])) ) )  
        self.datasets['train'] = self.datasets['train'].batch(train_batch_size)
        self.datasets['train'] = self.datasets['train'].prefetch(2)
        self.datasets['train'] = self.datasets['train'].repeat()
        self.iterators['train'] = self.datasets['train'].make_initializable_iterator()

        self.datasets['dev'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
        map(lambda x, y, r, z: ((x/255.), y, (r/255.), tf.squeeze(tf.reshape(tf.pad(tf.reshape(z, [-1, 28, 28])/255., [[0,0], [0, 36], [0, 36]]), [-1, 64*64])) ) )
        self.datasets['dev'] = self.datasets['dev'].batch(test_batch_size)
        self.datasets['dev'] = self.datasets['dev'].prefetch(2)
        self.iterators['dev'] = self.datasets['dev'].make_initializable_iterator()

        self.datasets['test'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
        map( lambda x, y, r, z: ((x/255.), y, (r/255.), tf.squeeze(tf.reshape(tf.pad(tf.reshape(z, [-1, 28, 28])/255., [[0,0], [0, 36], [0, 36]]), [-1, 64*64])) ) )
        self.datasets['test'] = self.datasets['test'].batch(test_batch_size)
        self.datasets['test'] = self.datasets['test'].prefetch(2)
        self.iterators['test'] = self.datasets['test'].make_initializable_iterator()

        self.next_elements = {}
        self.next_elements['train'] = self.iterators['train'].get_next()
        self.next_elements['dev'] = self.iterators['dev'].get_next()
        self.next_elements['test'] = self.iterators['test'].get_next()

    def init_iterator(self, sess, data_set):
        if data_set == 'train':
            sess.run(self.iterators['train'].initializer, feed_dict={'mnistbackground-box2_samples:0': self.train_data[0],
                                          'mnistground-box2_labels:0': self.train_data[-1], 
                                          'mnistbackground-box2_bsamples:0': self.train_data[1],
                                          'mnistbackground-box2_fsamples:0': self.train_data[2]})
        elif data_set == 'dev':
            sess.run(self.iterators['dev'].initializer, feed_dict={'mnistbackground-box2_samples:0': self.dev_data[0],
                                          'mnistground-box2_labels:0': self.dev_data[-1],
                                          'mnistbackground-box2_bsamples:0': self.dev_data[1],
                                          'mnistbackground-box2_fsamples:0': self.dev_data[2]})
        elif data_set == 'test':
            sess.run(self.iterators['test'].initializer, feed_dict={'mnistbackground-box2_samples:0': self.test_data[0],
                                         'mnistground-box2_labels:0': self.test_data[-1],
                                         'mnistbackground-box2_bsamples:0': self.test_data[1],
                                         'mnistbackground-box2_fsamples:0': self.test_data[2]})

class MnistBackgroundBox3_datasets():
    #with translation, rotation, or scale
    def __init__(self, train_batch_size, test_batch_size, scale_dataset=True, label_ind_dataset=False):
        if scale_dataset == True:
            filepath = '/tmp/mnist_background_box3.pkl.gz'
            url = 'http://www.cs.unc.edu/~ycharn/data/mnist_background_box3.pkl.gz'
        else:
            filepath = '/tmp/mnist_background_box3_noscale.pkl.gz'
            url = 'http://www.cs.unc.edu/~ycharn/data/mnist_background_box3_noscale.pkl.gz'

        if not os.path.isfile(filepath):
            print "Couldn't find MNIST-background-box3 dataset in /tmp, downloading..."
            urllib.urlretrieve(url, filepath)

        with gzip.open(filepath, 'rb') as f:
            self.train_data, self.dev_data, self.test_data = pickle.load(f)
        tmp = np.bincount(self.train_data[3])
        self.train_lab_dist = np.bincount(self.train_data[3])/float(self.train_data[3].shape[0])
        random_shuffle_data(self.train_data)
        random_shuffle_data(self.dev_data)
        #random_shuffle_data(self.test_data)

        data_ph = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-box3_samples')
        label_ph = tf.placeholder(self.train_data[-1].dtype, [None], name='mnistground-box3_labels')
        bdata_ph = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-box3_bsamples')
        fdata_ph = tf.placeholder(tf.float32, [None, 784], name='mnistbackground-box3_fsamples')
        self.datasets = {}
        self.iterators = {}

        self.datasets['train'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
        map( lambda x, y, r, z: ( (x/255.), y, (r/255.), tf.squeeze(tf.reshape(tf.pad(tf.reshape(z, [-1, 28, 28])/255., [[0,0], [0, 36], [0, 36]]), [-1, 64*64])) ) )  
        self.datasets['train'] = self.datasets['train'].batch(train_batch_size)
        self.datasets['train'] = self.datasets['train'].prefetch(2)
        self.datasets['train'] = self.datasets['train'].repeat()
        self.iterators['train'] = self.datasets['train'].make_initializable_iterator()

        self.datasets['dev'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
        map(lambda x, y, r, z: ((x/255.), y, (r/255.), tf.squeeze(tf.reshape(tf.pad(tf.reshape(z, [-1, 28, 28])/255., [[0,0], [0, 36], [0, 36]]), [-1, 64*64])) ) )
        self.datasets['dev'] = self.datasets['dev'].batch(test_batch_size)
        self.datasets['dev'] = self.datasets['dev'].prefetch(2)
        self.iterators['dev'] = self.datasets['dev'].make_initializable_iterator()

        self.datasets['test'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
        map( lambda x, y, r, z: ((x/255.), y, (r/255.), tf.squeeze(tf.reshape(tf.pad(tf.reshape(z, [-1, 28, 28])/255., [[0,0], [0, 36], [0, 36]]), [-1, 64*64])) ) )
        self.datasets['test'] = self.datasets['test'].batch(test_batch_size)
        self.datasets['test'] = self.datasets['test'].prefetch(2)
        self.iterators['test'] = self.datasets['test'].make_initializable_iterator()

        self.next_elements = {}
        self.next_elements['train'] = self.iterators['train'].get_next()
        self.next_elements['dev'] = self.iterators['dev'].get_next()
        self.next_elements['test'] = self.iterators['test'].get_next()

        if label_ind_dataset == True:
            self.train_data_dict = {}
            self.dev_data_dict = {}
            self.test_data_dict = {}
            for i in xrange(10):
                c_train = 'train_{}'.format(i)
                c_dev = 'dev_{}'.format(i)
                c_test = 'test_{}'.format(i)
                self.datasets[c_train] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
                map( lambda x, y, r, z: ( (x/255.), y, (r/255.), tf.squeeze(tf.reshape(tf.pad(tf.reshape(z, [-1, 28, 28])/255., [[0,0], [0, 36], [0, 36]]), [-1, 64*64])) ) )  
                self.datasets[c_train] = self.datasets[c_train].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_train] = self.datasets[c_train].prefetch(2)
                self.datasets[c_train] = self.datasets[c_train].repeat()
                self.iterators[c_train] = self.datasets[c_train].make_initializable_iterator()
                self.next_elements[c_train] = self.iterators[c_train].get_next()
                self.datasets[c_dev] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
                map(lambda x, y, r, z: ((x/255.), y, (r/255.), tf.squeeze(tf.reshape(tf.pad(tf.reshape(z, [-1, 28, 28])/255., [[0,0], [0, 36], [0, 36]]), [-1, 64*64])) ) )
                self.datasets[c_dev] = self.datasets[c_dev].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_dev] = self.datasets[c_dev].prefetch(1)
                self.datasets[c_dev] = self.datasets[c_dev].repeat()
                self.iterators[c_dev] = self.datasets[c_dev].make_initializable_iterator()   
                self.next_elements[c_dev] = self.iterators[c_dev].get_next()            
                self.datasets[c_test] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph, bdata_ph, fdata_ph)).\
                map( lambda x, y, r, z: ((x/255.), y, (r/255.), tf.squeeze(tf.reshape(tf.pad(tf.reshape(z, [-1, 28, 28])/255., [[0,0], [0, 36], [0, 36]]), [-1, 64*64])) ) )
                self.datasets[c_test] = self.datasets[c_test].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
                self.datasets[c_test] = self.datasets[c_test].prefetch(1)
                self.datasets[c_test] = self.datasets[c_test].repeat()
                self.iterators[c_test] = self.datasets[c_test].make_initializable_iterator() 
                self.next_elements[c_test] = self.iterators[c_test].get_next()    

                self.train_data_dict[i] = (self.train_data[0][self.train_data[3]==i], 
                                           self.train_data[1][self.train_data[3]==i],
                                           self.train_data[2][self.train_data[3]==i])
                self.dev_data_dict[i] = (self.dev_data[0][self.dev_data[3]==i],
                                         self.dev_data[1][self.dev_data[3]==i],
                                         self.dev_data[2][self.dev_data[3]==i])
                self.test_data_dict[i] = (self.test_data[0][self.test_data[3]==i],
                                          self.test_data[1][self.test_data[3]==i],
                                          self.test_data[2][self.test_data[3]==i])

    def init_iterator(self, sess, data_set):
        if not data_set[-1].isdigit():
            if data_set == 'train':
                c_data = self.train_data
            elif data_set == 'dev':
                c_data = self.dev_data
            elif data_set == 'test':
                c_data = self.test_data
            sess.run(self.iterators['train'].initializer, feed_dict={'mnistbackground-box3_samples:0': self.c_data[0],
                                          'mnistground-box3_labels:0': self.c_data[-1], 
                                          'mnistbackground-box3_bsamples:0': self.c_data[1],
                                          'mnistbackground-box3_fsamples:0': self.c_data[2]})
        else:
            c_lab = int(data_set[-1])
            if data_set.startswith('train'):
                c_data = self.train_data_dict[c_lab]
            elif data_set.startswith('dev'):
                c_data = self.dev_data_dict[c_lab]
            elif data_set.startswith('test'):
                c_data = self.test_data_dict[c_lab]
            c_lab = np.ones((c_data[0].shape[0]), dtype=self.train_data[3].dtype)*c_lab
            sess.run(self.iterators[data_set].initializer, feed_dict={'mnistbackground-box3_samples:0': c_data[0],
                                          'mnistground-box3_labels:0': c_lab, 
                                          'mnistbackground-box3_bsamples:0': c_data[1],
                                          'mnistbackground-box3_fsamples:0': c_data[2]})

class MnistBackground2objs_datasets():
    #with translation, rotation, or scale
    def __init__(self, train_batch_size, test_batch_size, label_ind_dataset=False, random_data=True, cat=None):

        if not cat:
            if random_data:
                filepath = '/tmp/mnist_background_twobojs_random.pkl.gz'
                url = 'http://www.cs.unc.edu/~ycharn/data/mnist_background_twobojs_random.pkl.gz'
            else:
                filepath = '/tmp/mnist_background_twobojs.pkl.gz'
                url = 'http://www.cs.unc.edu/~ycharn/data/mnist_background_twobojs.pkl.gz'
        else:
            if cat == '01':
                filepath = '/tmp/mnist_background_twobojs_cat01_random.pkl.gz'
                url = 'http://www.cs.unc.edu/~ycharn/data/mnist_background_twobojs_cat01_random.pkl.gz'                

        if not os.path.isfile(filepath):
            if random_data:
                print "Couldn't find MNIST-background-two-objects-random dataset in /tmp, downloading..."
            else:
                print "Couldn't find MNIST-background-two-objects dataset in /tmp, downloading..."
            urllib.urlretrieve(url, filepath)

        with gzip.open(filepath, 'rb') as f:
            self.train_data, self.dev_data, self.test_data = pickle.load(f)
        random_shuffle_data(self.train_data)
        random_shuffle_data(self.dev_data)
        random_shuffle_data(self.test_data)

        data_ph1 = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-2objs_samples_1')
        data_ph2 = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-2objs_samples_2')
        bdata_ph = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-2objs_bsamples')
        fdata_ph1 = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-2objs_fsamples_1')
        fdata_ph2 = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-2objs_fsamples_2')
        self.datasets = {}
        self.iterators = {}

        self.datasets['train'] = tf.data.Dataset.from_tensor_slices((data_ph1, data_ph2, bdata_ph, fdata_ph1, fdata_ph2)).\
        map( lambda x, y, r, z, k: ( (x/255.), (y/255.), (r/255.), (z/255.), (k/255.)) )
        self.datasets['train'] = self.datasets['train'].shuffle(buffer_size=self.train_data[0].shape[0])
        self.datasets['train'] = self.datasets['train'].batch(train_batch_size)
        self.datasets['train'] = self.datasets['train'].prefetch(2)
        self.datasets['train'] = self.datasets['train'].repeat()
        self.iterators['train'] = self.datasets['train'].make_initializable_iterator()

        self.datasets['dev'] = tf.data.Dataset.from_tensor_slices((data_ph1, data_ph2, bdata_ph, fdata_ph1, fdata_ph2)).\
        map( lambda x, y, r, z, k: ( (x/255.), (y/255.), (r/255.), (z/255.), (k/255.)) ) 
        self.datasets['dev'] = self.datasets['dev'].shuffle(buffer_size=self.dev_data[0].shape[0])
        self.datasets['dev'] = self.datasets['dev'].batch(test_batch_size)
        self.datasets['dev'] = self.datasets['dev'].prefetch(2)
        self.iterators['dev'] = self.datasets['dev'].make_initializable_iterator()

        self.datasets['test'] = tf.data.Dataset.from_tensor_slices((data_ph1, data_ph2, bdata_ph, fdata_ph1, fdata_ph2)).\
        map( lambda x, y, r, z, k: ( (x/255.), (y/255.), (r/255.), (z/255.), (k/255.)) ) 
        self.datasets['test'] = self.datasets['test'].shuffle(buffer_size=self.test_data[0].shape[0])
        self.datasets['test'] = self.datasets['test'].batch(test_batch_size)
        self.datasets['test'] = self.datasets['test'].prefetch(2)
        self.iterators['test'] = self.datasets['test'].make_initializable_iterator()

        self.next_elements = {}
        self.next_elements['train'] = self.iterators['train'].get_next()
        self.next_elements['dev'] = self.iterators['dev'].get_next()
        self.next_elements['test'] = self.iterators['test'].get_next()


    def init_iterator(self, sess, data_set):
        if data_set == 'train':
            c_data = self.train_data
        elif data_set == 'dev':
            c_data = self.dev_data
        elif data_set == 'test':
            c_data = self.test_data
        sess.run(self.iterators[data_set].initializer, feed_dict={'mnistbackground-2objs_samples_1:0': c_data[0][:, 0, :],
                                      'mnistbackground-2objs_samples_2:0': c_data[0][:, 1, :], 
                                      'mnistbackground-2objs_bsamples:0': c_data[1],
                                      'mnistbackground-2objs_fsamples_1:0': c_data[2][:, 0, :],
                                      'mnistbackground-2objs_fsamples_2:0': c_data[2][:, 1, :]})

class MnistBackground3objs_datasets():
    #with translation, rotation, or scale
    def __init__(self, train_batch_size, test_batch_size, label_ind_dataset=False):


        filepath = '/tmp/mnist_background_threebojs_cat012_random.pkl.gz'
        url = 'http://www.cs.unc.edu/~ycharn/data/mnist_background_threebojs_cat012_random.pkl.gz'                

        if not os.path.isfile(filepath):
            print "Couldn't find mnist_background_threebojs_cat012_random dataset in /tmp, downloading..."
            urllib.urlretrieve(url, filepath)

        with gzip.open(filepath, 'rb') as f:
            self.train_data, self.dev_data, self.test_data = pickle.load(f)
        random_shuffle_data(self.train_data)
        random_shuffle_data(self.dev_data)
        random_shuffle_data(self.test_data)

        data_ph1 = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-3objs_samples_1')
        data_ph2 = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-3objs_samples_2')
        bdata_ph = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-3objs_bsamples')
        fdata_ph1 = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-3objs_fsamples_1')
        fdata_ph2 = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-3objs_fsamples_2')
        fdata_ph3 = tf.placeholder(tf.float32, [None, 64*64], name='mnistbackground-3objs_fsamples_3')
        self.datasets = {}
        self.iterators = {}

        self.datasets['train'] = tf.data.Dataset.from_tensor_slices((data_ph1, data_ph2, bdata_ph, fdata_ph1, fdata_ph2, fdata_ph3)).\
        map( lambda x, y, r, z, k, m: ( (x/255.), (y/255.), (r/255.), (z/255.), (k/255.), (m/255.)) )
        self.datasets['train'] = self.datasets['train'].shuffle(buffer_size=self.train_data[0].shape[0])
        self.datasets['train'] = self.datasets['train'].batch(train_batch_size)
        self.datasets['train'] = self.datasets['train'].prefetch(2)
        self.datasets['train'] = self.datasets['train'].repeat()
        self.iterators['train'] = self.datasets['train'].make_initializable_iterator()

        self.datasets['dev'] = tf.data.Dataset.from_tensor_slices((data_ph1, data_ph2, bdata_ph, fdata_ph1, fdata_ph2, fdata_ph3)).\
        map( lambda x, y, r, z, k, m: ( (x/255.), (y/255.), (r/255.), (z/255.), (k/255.), (m/255.)) ) 
        self.datasets['dev'] = self.datasets['dev'].shuffle(buffer_size=self.dev_data[0].shape[0])
        self.datasets['dev'] = self.datasets['dev'].batch(test_batch_size)
        self.datasets['dev'] = self.datasets['dev'].prefetch(2)
        self.iterators['dev'] = self.datasets['dev'].make_initializable_iterator()

        self.datasets['test'] = tf.data.Dataset.from_tensor_slices((data_ph1, data_ph2, bdata_ph, fdata_ph1, fdata_ph2, fdata_ph3)).\
        map( lambda x, y, r, z, k, m: ( (x/255.), (y/255.), (r/255.), (z/255.), (k/255.), (m/255.)) ) 
        self.datasets['test'] = self.datasets['test'].shuffle(buffer_size=self.test_data[0].shape[0])
        self.datasets['test'] = self.datasets['test'].batch(test_batch_size)
        self.datasets['test'] = self.datasets['test'].prefetch(2)
        self.iterators['test'] = self.datasets['test'].make_initializable_iterator()

        self.next_elements = {}
        self.next_elements['train'] = self.iterators['train'].get_next()
        self.next_elements['dev'] = self.iterators['dev'].get_next()
        self.next_elements['test'] = self.iterators['test'].get_next()


    def init_iterator(self, sess, data_set):
        if data_set == 'train':
            c_data = self.train_data
        elif data_set == 'dev':
            c_data = self.dev_data
        elif data_set == 'test':
            c_data = self.test_data
        sess.run(self.iterators[data_set].initializer, feed_dict={'mnistbackground-3objs_samples_1:0': c_data[0][:, 0, :],
                                      'mnistbackground-3objs_samples_2:0': c_data[0][:, 1, :], 
                                      'mnistbackground-3objs_bsamples:0': c_data[1],
                                      'mnistbackground-3objs_fsamples_1:0': c_data[2][:, 0, :],
                                      'mnistbackground-3objs_fsamples_2:0': c_data[2][:, 1, :],
                                      'mnistbackground-3objs_fsamples_3:0': c_data[2][:, 2, :]})

class MnistBackgroundLab97_datasets():
    def __init__(self, train_batch_size, test_batch_size):
        filepath = '/tmp/mnist_background_lab97.pkl.gz'
        url = 'http://www.cs.unc.edu/~ycharn/data/mnist_background_lab97.pkl.gz'

        if not os.path.isfile(filepath):
            print "Couldn't find MNIST-background-label97 dataset in /tmp, downloading..."
            urllib.urlretrieve(url, filepath)

        with gzip.open('/tmp/mnist_background_lab97.pkl.gz', 'rb') as f:
            self.train_data, self.dev_data, self.test_data = pickle.load(f)
        random_shuffle_data(self.train_data)
        random_shuffle_data(self.dev_data)
        random_shuffle_data(self.test_data)

        data_ph = tf.placeholder(self.train_data[0].dtype, [None, 784], name='mnist_samples')
        label_ph = tf.placeholder(tf.int32, [None], name='mnist_labels')
        self.datasets = {}
        self.iterators = {}

        self.datasets['train'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph))
        self.datasets['train'] = self.datasets['train'].batch(train_batch_size)
        self.datasets['train'] = self.datasets['train'].prefetch(1)
        self.datasets['train'] = self.datasets['train'].repeat()
        self.iterators['train'] = self.datasets['train'].make_initializable_iterator()

        self.datasets['dev'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph))
        self.datasets['dev'] = self.datasets['dev'].batch(test_batch_size)
        self.datasets['dev'] = self.datasets['dev'].prefetch(1)
        self.iterators['dev'] = self.datasets['dev'].make_initializable_iterator()

        self.datasets['test'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph))
        self.datasets['test'] = self.datasets['test'].batch(test_batch_size)
        self.datasets['test'] = self.datasets['test'].prefetch(1)
        self.iterators['test'] = self.datasets['test'].make_initializable_iterator()

        self.next_elements = {}
        self.next_elements['train'] = self.iterators['train'].get_next()
        self.next_elements['dev'] = self.iterators['dev'].get_next()
        self.next_elements['test'] = self.iterators['test'].get_next()

    def init_iterator(self, sess, data_set):
        if data_set == 'train':
            sess.run(self.iterators['train'].initializer, feed_dict={'mnist_samples:0': self.train_data[0],
                                          'mnist_labels:0': self.train_data[1]})
        elif data_set == 'dev':
            sess.run(self.iterators['dev'].initializer, feed_dict={'mnist_samples:0': self.dev_data[0],
                                          'mnist_labels:0': self.dev_data[1]})
        elif data_set == 'test':
            sess.run(self.iterators['test'].initializer, feed_dict={'mnist_samples:0': self.test_data[0],
                                          'mnist_labels:0': self.test_data[1]})

class SVHN_extra_dataset():
    def __init__(self, train_batch_size, test_batch_size, data_dir):
        self.train_data = read_all_data(['SVHN_e_train_0.p'], data_dir)
        self.test_data = read_all_data(['SVHN_e_train_1.p'], data_dir)
        random_shuffle_data(self.train_data)
        random_shuffle_data(self.test_data)

        data_ph = tf.placeholder(self.train_data[0].dtype, [None, 3072], name='svhn_samples')
        label_ph = tf.placeholder(tf.int32, [None], name='svhn_labels')
        self.datasets = {}
        self.iterators = {}

        self.datasets['train'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph)).map(lambda x, y: (2*(tf.cast(x, tf.float32)/255.-.5), y))
        self.datasets['train'] = self.datasets['train'].batch(train_batch_size)
        self.datasets['train'] = self.datasets['train'].prefetch(1)
        self.datasets['train'] = self.datasets['train'].repeat()
        self.iterators['train'] = self.datasets['train'].make_initializable_iterator()

        self.datasets['test'] = tf.data.Dataset.from_tensor_slices((data_ph, label_ph)).map(lambda x, y: (2*(tf.cast(x, tf.float32)/255.-.5), y))
        self.datasets['test'] = self.datasets['test'].batch(test_batch_size)
        self.datasets['test'] = self.datasets['test'].prefetch(1)
        self.iterators['test'] = self.datasets['test'].make_initializable_iterator()

        self.next_elements = {}
        self.next_elements['train'] = self.iterators['train'].get_next()
        self.next_elements['test'] = self.iterators['test'].get_next()

    def init_iterator(self, sess, data_set):
        if data_set == 'train':
            sess.run(self.iterators['train'].initializer, feed_dict={'svhn_samples:0': self.train_data[0],
                                          'svhn_labels:0': self.train_data[1]})
        elif data_set == 'test':
            sess.run(self.iterators['test'].initializer, feed_dict={'svhn_samples:0': self.test_data[0],
                                          'svhn_labels:0': self.test_data[1]})



class IMDB_sent_embs_datasets():
    def __init__(self, train_batch_size, test_batch_size):
        filepath = '/tmp/exp6.rep'
        url = 'http://www.cs.unc.edu/~ycharn/data/exp6.rep'

        if not os.path.isfile(filepath):
            print "Couldn't find imdb-sent-embs dataset in /tmp, downloading..."
            urllib.urlretrieve(url, filepath)

        tmp = np.load(open(filepath, 'rb'))
        tmp = tmp['arr_0']
        tmp = tmp.reshape(tmp.shape[0]/2, 2, 900).reshape(tmp.shape[0]/2, 2*900)
        self.train_data = tmp[:-2000,:]
        self.dev_data = tmp[-2000:-1000, :]
        self.test_data = tmp[-1000:, :]
        #random_shuffle_data(self.train_data)
        #random_shuffle_data(self.dev_data)
        #random_shuffle_data(self.test_data)

        data_ph = tf.placeholder(self.train_data[0].dtype, [None, 2*900], name='imdb_samples')
        self.datasets = {}
        self.iterators = {}
        self.next_elements = {}

        self.datasets['train'] = tf.data.Dataset.from_tensor_slices((data_ph))
        self.datasets['train'] = self.datasets['train'].apply(tf.contrib.data.batch_and_drop_remainder(train_batch_size))
        self.datasets['train'] = self.datasets['train'].prefetch(1)
        self.datasets['train'] = self.datasets['train'].repeat()
        self.iterators['train'] = self.datasets['train'].make_initializable_iterator()
        self.next_elements['train'] = self.iterators['train'].get_next()

        self.datasets['dev'] = tf.data.Dataset.from_tensor_slices((data_ph))
        self.datasets['dev'] = self.datasets['dev'].apply(tf.contrib.data.batch_and_drop_remainder(test_batch_size))
        self.datasets['dev'] = self.datasets['dev'].prefetch(1)
        self.iterators['dev'] = self.datasets['dev'].make_initializable_iterator()
        self.next_elements['dev'] = self.iterators['dev'].get_next()

        self.datasets['test'] = tf.data.Dataset.from_tensor_slices((data_ph))
        self.datasets['test'] = self.datasets['test'].apply(tf.contrib.data.batch_and_drop_remainder(test_batch_size))
        self.datasets['test'] = self.datasets['test'].prefetch(1)
        self.iterators['test'] = self.datasets['test'].make_initializable_iterator() 
        self.next_elements['test'] = self.iterators['test'].get_next()

    def init_iterator(self, sess, data_set):
        if not data_set[-1].isdigit():
            if data_set == 'train':
                c_data = self.train_data
            elif data_set == 'dev':
                c_data = self.dev_data
            elif data_set == 'test':
                c_data = self.test_data
            sess.run(self.iterators[data_set].initializer, feed_dict={'imdb_samples:0': c_data})                
