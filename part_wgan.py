import tflib.Datasets_tff
import tflib.ops.linear
import tflib.ops.deconv2d
import tflib.ops.conv2d
import tflib.save_images
import tensorflow as tf
from tensorflow.contrib.training import HParams 
import numpy as np
import dateutil.tz
import datetime
import getpass
import tflib.plot
import os
import matplotlib
import collections
matplotlib.use('Agg')
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator
import matplotlib.pyplot as plt
from timeit import default_timer as timer
import json
import argparse

def read_config(file_path):
    """Read JSON config."""
    json_object = json.load(open(file_path, 'r'))
    return json_object
parser = argparse.ArgumentParser(description='traning mnist-background or fashion-mnist-background part model')
parser.add_argument(
    "-setting",
    help="path to the files with all parameter settings (json)",
    required=True
)
parser.add_argument(
    "-sampling",
    help="if ony sampling from the given model",
    dest='sampling', 
    action='store_true'
)
parser.add_argument(
    "-output",
    help="path where the program saves outputs",
    dest='output', 
    default=""
)
args = parser.parse_args()
settings = read_config(args.setting)
if args.sampling:
    SamplingConfig = collections.namedtuple('Configuration', [
    'sampling_times', 'sampling_rounds', 'sampling_outfile_name'])
    sampling_configs = SamplingConfig(sampling_times=settings['sampling_times'],
                                      sampling_rounds=settings['sampling_rounds'],
                                      sampling_outfile_name=settings['sampling_outfile_name'])
    print(sampling_configs)


Configuration = collections.namedtuple('Configuration', [
    'eval_only', 'train_object', 'model_path', 'output_dim', 
    'v_size', 'eval_while_training', 'label_used',
    'dataset', "fg_Gnoise"
])
configs = Configuration(eval_only=settings['eval_only'], 
                        train_object=settings['training_objective'], #mnist-f (mnist), mnist-b (background), mnist-c (mnist+background)
                        model_path=settings['model_path'], #pretrained model_path to be loaded, false to not load anything
                        #sep_saving_flag=True, #separate the model file for discriminator and generator
                        output_dim=settings['data']['output_dim'], #28*28 for mnist data, 64*64 for mnist-background-box* data
                        v_size=settings['v_size'], #size of images for visualization at once
                        eval_while_training=settings['eval_while_training'], #evaluate test w-dist while training
                        label_used = settings['data']['label_used'], #used label in mnist-background dataset, -1 = using all,
                        dataset= settings['data']['dataset'], #mnist-background, mnist-background-box1, mnist-background-box2, mnist-background-box3, mnist-background-box3-noscale
                        fg_Gnoise=settings['data']['fg_Gnoise'] #add noise to the foreground images
                        )
#regularize some configs
if configs.eval_only==True:
    configs=configs._replace(eval_while_training=False)
if configs.dataset == 'mnist-background' or configs.dataset == 'fashion-mnist-background':
    configs=configs._replace(output_dim=28*28)
elif configs.dataset == 'mnist-background-box2' or \
   configs.dataset == 'mnist-background-box3' or \
   configs.dataset == 'mnist-background-box3-noscale':
   configs=configs._replace(output_dim=64*64)
hparams = HParams(batch_size=settings['hyperparams']['batch_size'], #batch size for training
                  code_size=settings['hyperparams']['code_size'], #noise code size
                  LAMBDA=settings['hyperparams']['LAMBDA'],
                  iters=settings['hyperparams']['iters'],
                  disc_iters=settings['hyperparams']['disc_iters'],
                  disc_dim=settings['hyperparams']['disc_dim'],
                  gen_dim=settings['hyperparams']['gen_dim'],
                  noise_type=settings['hyperparams']['noise_type'], #Gaussian, Uniform
                  disc_learning_rate=settings['hyperparams']['disc_learning_rate'],
                  gen_learning_rate=settings['hyperparams']['gen_learning_rate']
                  )
DATA_LEN = int(np.sqrt(configs.output_dim))
#generate output folder
c_user_name = getpass.getuser()
now = datetime.datetime.now(dateutil.tz.tzlocal())
timestamp = now.strftime('%Y_%m_%d_%H_%M_%S')
if args.output == '':
    out_basedir = "/home/{}/{}/{}/".format(c_user_name, configs.dataset, configs.train_object) + timestamp #folder to output result images
else:
    out_basedir = "{}".format(args.output)# + timestamp 
hparams.out_basedir = out_basedir
if configs.eval_only == False:
    if not os.path.exists(out_basedir):
        os.makedirs(out_basedir)
print('out folder = {}'.format(out_basedir))
print('-'*10+'Configurations'+'-'*10)
print(configs)
print('-'*30)
print('-'*10+'hyper-parameters'+'-'*10)
print(hparams)
print('-'*30)

#loading dataset
if configs.dataset == 'mnist-background':
    if configs.label_used == -1:
        MnistB = tflib.Datasets_tff.MnistBackground_datasets(hparams.batch_size, hparams.batch_size, configs.fg_Gnoise)
    else:
        MnistB = tflib.Datasets_tff.MnistBackground_datasets(hparams.batch_size, hparams.batch_size, configs.fg_Gnoise, label_ind_dataset=True)
elif configs.dataset == 'fashion-mnist-background':
    if configs.label_used == -1:
        MnistB = tflib.Datasets_tff.FashionMnistBackground_datasets(hparams.batch_size, hparams.batch_size, configs.fg_Gnoise)
    else:
        MnistB = tflib.Datasets_tff.FashionMnistBackground_datasets(hparams.batch_size, hparams.batch_size, configs.fg_Gnoise, label_ind_dataset=True)
elif configs.dataset == 'mnist-background-box1':
    MnistB = tflib.Datasets_tff.MnistBackgroundBox1_datasets(hparams.batch_size, hparams.batch_size)
elif configs.dataset == 'mnist-background-box2':
    MnistB = tflib.Datasets_tff.MnistBackgroundBox2_datasets(hparams.batch_size, hparams.batch_size)
elif configs.dataset == 'mnist-background-box3':
    MnistB = tflib.Datasets_tff.MnistBackgroundBox3_datasets(hparams.batch_size, hparams.batch_size)
elif configs.dataset == 'mnist-background-box3-noscale':
    if configs.label_used == -1:
        MnistB = tflib.Datasets_tff.MnistBackgroundBox3_datasets(hparams.batch_size, hparams.batch_size, scale_dataset=False)
    else:
         MnistB = tflib.Datasets_tff.MnistBackgroundBox3_datasets(hparams.batch_size, hparams.batch_size, scale_dataset=False, label_ind_dataset=True)

def custom_concat(axis, input):
    if tf.__version__[0] == '0':
        return tf.concat(axis, input)
    elif tf.__version__[0] == '1':
        return tf.concat(input, axis)  

def generate_g_var(batch_size, dropout_flag, retain_prob=None):
    if hparams.noise_type == 'Gaussian':
        codes = np.random.normal(size=(batch_size, hparams.code_size)).astype(np.float32)
    elif hparams.noise_type == 'Uniform':
        codes = codes = np.random.uniform(-1, 1, size=(batch_size, hparams.code_size)).astype(np.float32)
    return codes

def DCGen32(name, noise, code_size, dim):
    output = tflib.ops.linear.Linear(name+'.L1', code_size, 4*4*4*dim, noise)
    output = tf.nn.relu(output)
    output = tf.reshape(output, [-1, 4*dim, 4, 4])

    output = tflib.ops.deconv2d.Deconv2D(name+'.L2', 4*dim, 2*dim, 5, output)
    output = tf.nn.relu(output)
    output = output[:,:,:7,:7]

    output = tflib.ops.deconv2d.Deconv2D(name+'.L3', 2*dim, dim, 5, output)
    output = tf.nn.relu(output)

    output = tflib.ops.deconv2d.Deconv2D(name+'.L4', dim, 1, 5, output)
    output = tf.nn.sigmoid(output)

    return tf.reshape(output, [-1, configs.output_dim])

def DCGen64(name, noise, code_size, dim):
    output = tflib.ops.linear.Linear(name+'.L1', code_size, 8*4*4*dim, noise)
    output = tf.nn.relu(output)
    output = tf.reshape(output, [-1, 8*dim, 4, 4])

    output = tflib.ops.deconv2d.Deconv2D(name+'.L2', 8*dim, 4*dim, 5, output)
    output = tf.nn.relu(output)

    output = tflib.ops.deconv2d.Deconv2D(name+'.L3', 4*dim, 2*dim, 5, output)
    output = tf.nn.relu(output)

    output = tflib.ops.deconv2d.Deconv2D(name+'.L4', 2*dim, dim, 5, output)
    output = tf.nn.relu(output)

    output = tflib.ops.deconv2d.Deconv2D(name+'.L5', dim, 1, 5, output)
    output = tf.nn.sigmoid(output)
   
    return tf.reshape(output, [-1, configs.output_dim]) 

def DCDisc32(name, inputs, dim):
    output = tf.reshape(inputs, [-1, 1, DATA_LEN, DATA_LEN])

    output = tflib.ops.conv2d.Conv2D(name+'.L1', 1, dim, 5, output, stride=2)
    output = tf.nn.leaky_relu(output)

    output = tflib.ops.conv2d.Conv2D(name+'.L2', dim, 2*dim, 5, output, stride=2)
    output = tf.nn.leaky_relu(output)

    output = tflib.ops.conv2d.Conv2D(name+'.L3', 2*dim, 4*dim, 5, output, stride=2)
    output = tf.nn.leaky_relu(output)

    output = tf.reshape(output, [-1, 4*4*4*dim])
    output = tflib.ops.linear.Linear(name+'.L4', 4*4*4*dim, 1, output)

    return tf.reshape(output, [-1])

def DCDisc64(name, inputs, dim):
    output = tf.reshape(inputs, [-1, 1, DATA_LEN, DATA_LEN])

    output = tflib.ops.conv2d.Conv2D(name+'.L1', 1, dim, 5, output, stride=2)
    output = tf.nn.leaky_relu(output)

    output = tflib.ops.conv2d.Conv2D(name+'.L2', dim, 2*dim, 5, output, stride=2)
    output = tf.nn.leaky_relu(output)

    output = tflib.ops.conv2d.Conv2D(name+'.L3', 2*dim, 4*dim, 5, output, stride=2)
    output = tf.nn.leaky_relu(output)

    output = tflib.ops.conv2d.Conv2D(name+'.L4', 4*dim, 8*dim, 5, output, stride=2)
    output = tf.nn.leaky_relu(output)

    output = tf.reshape(output, [-1, 8*4*4*dim])
    output = tflib.ops.linear.Linear(name+'.L5', 8*4*4*dim, 1, output)

    return tf.reshape(output, [-1])    

def Construct_loss_and_penalty(disc_name, real_data, fake_data):
    alpha = tf.random_uniform(
        shape=[hparams.batch_size,1], 
        minval=0.,
        maxval=1.
    )
    differences = fake_data - real_data
    interpolates = real_data + (alpha*differences)
    disc_real = Discriminator(disc_name, real_data, hparams.disc_dim)
    disc_fake = Discriminator(disc_name, fake_data, hparams.disc_dim)
    inter_vals = Discriminator(disc_name, interpolates, hparams.disc_dim)
    gradients = tf.gradients(inter_vals, [interpolates])[0]
    slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), reduction_indices=[1]))
    #gradient_penalty = tf.reduce_mean((slopes-1.)**2)
    gradient_penalty = tf.reduce_mean(tf.maximum(0., slopes-1.)**2)
    gen_cost = -tf.reduce_mean(disc_fake)
    disc_cost = tf.reduce_mean(disc_fake) - tf.reduce_mean(disc_real) 
    wdist = -disc_cost
    disc_cost += hparams.LAMBDA*gradient_penalty
    return disc_cost, wdist, gradient_penalty, gen_cost


#real_data = tf.placeholder(tf.float32, shape=[hparams.batch_size, configs.output_dim], name='{}_real_data'.format(configs.train_object))
#g_var = tf.placeholder(tf.float32, shape=[hparams.batch_size, hparams.code_size], name='g_var_phB')
if configs.label_used != -1:
    train_dname = 'train_{}'.format(configs.label_used)
else:
    train_dname = 'train'
real_data_vec = MnistB.next_elements[train_dname]
if configs.train_object ==  'mnist-f':
    real_data = real_data_vec[3]
elif configs.train_object == 'mnist-b':
    real_data = real_data_vec[2]
elif configs.train_object == 'mnist-c':
    real_data = real_data_vec[0]

if configs.label_used != -1:
    dev_dname = 'dev_{}'.format(configs.label_used)
else:
    dev_dname = 'dev'
dev_real_data_vec = MnistB.next_elements[dev_dname]
if configs.train_object ==  'mnist-f':
    dev_real_data = dev_real_data_vec[3]
elif configs.train_object == 'mnist-b':
    dev_real_data = dev_real_data_vec[2]
elif configs.train_object == 'mnist-c':
    dev_real_data = dev_real_data_vec[0]

if configs.dataset == 'mnist-background' or configs.dataset == 'fashion-mnist-background':
    Generator = DCGen32
    Discriminator = DCDisc32
elif configs.dataset == 'mnist-background-box2' or \
   configs.dataset == 'mnist-background-box3' or \
   configs.dataset == 'mnist-background-box3-noscale':
    Generator = DCGen64
    Discriminator = DCDisc64

gen_var_name = '{}_generator'.format(configs.train_object)
disc_var_name = '{}_discriminator'.format(configs.train_object)
ind_disc_var_name = '{}_ind_discriminator'.format(configs.train_object)

if hparams.noise_type == 'Gaussian':
    fake_data = Generator(gen_var_name, 
        tf.random_normal([hparams.batch_size, hparams.code_size]), 
        hparams.code_size, hparams.gen_dim)
elif hparams.noise_type == 'Uniform':
    fake_data = Generator(gen_var_name, 
        tf.random_uniform([hparams.batch_size, hparams.code_size], minval=-1, maxval=1),
        hparams.code_size, hparams.gen_dim)
tf.add_to_collection('{}_generator_out'.format(configs.train_object), fake_data)
# Gradient penalty samples
disc_cost, wdist, gradient_penalty, gen_cost = Construct_loss_and_penalty(disc_var_name, real_data, fake_data)

if configs.eval_only == True or configs.eval_while_training == True:
    ind_disc_cost, ind_wdist, ind_gradient_penalty, _ = Construct_loss_and_penalty(ind_disc_var_name, dev_real_data, fake_data)

gen_params = tflib.params_with_name(gen_var_name)
disc_params = tflib.params_with_name(disc_var_name)
gen_train_op = tf.train.AdamOptimizer(learning_rate=hparams.gen_learning_rate, beta1=0.5, beta2=0.9).minimize(gen_cost, var_list=gen_params)
disc_train_op = tf.train.AdamOptimizer(learning_rate=hparams.disc_learning_rate, beta1=0.5, beta2=0.9).minimize(disc_cost, var_list=disc_params)

if configs.eval_only == True or configs.eval_while_training == True:
    ind_disc_params = tflib.params_with_name(ind_disc_var_name)
    ind_optimizer = tf.train.AdamOptimizer(learning_rate=1e-3)
    ind_disc_train_op = ind_optimizer.minimize(ind_disc_cost, var_list=ind_disc_params)

# For generating samples
if hparams.noise_type == 'Gaussian':
    fixed_noise_array = np.random.normal(size=(configs.v_size, hparams.code_size)).astype('float32')
elif hparams.noise_type == 'Uniform':
    fixed_noise_array = np.random.uniform(size=(configs.v_size, hparams.code_size), low=-1).astype('float32')
fixed_noise = tf.constant(fixed_noise_array)
fixed_samples = Generator(gen_var_name, fixed_noise, hparams.code_size, hparams.gen_dim)

def generate_image(frame):
    samples = session.run(fixed_samples)
    tflib.save_images.save_images(
        samples.reshape((configs.v_size, DATA_LEN, DATA_LEN)), 
        os.path.join( out_basedir, 'samples_{}.png'.format(frame+1)), 'it = {}'.format(frame+1))

def generate_interpolate_imgs(interpolate_size = 10):
    template = fixed_noise_array[:10, :]
    inter_vecs = np.linspace(-3, 3, interpolate_size)
    for i in xrange(CODE_SIZE):
        inter_noises = []
        for j in xrange(interpolate_size):
            tmp = np.copy(template)
            tmp[:, i] = inter_vecs[j]
            inter_noises.extend(tmp)
        inter_noises = np.array(inter_noises)
        inter_samples = Generator(V_SIZE, noise=tf.constant(inter_noises))
        samples = session.run(inter_samples)
        tflib.save_images.save_images(
        samples.reshape((V_SIZE, DATA_LEN, DATA_LEN)), 
        os.path.join( out_basedir, 'inter_samples_c_{}.png'.format(i)) )

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=.19)
savers = {}
# if configs.sep_saving_flag == False:
#     savers['all'] = tf.train.Saver()
# else:
savers['gen'] = tf.train.Saver(gen_params)
savers['disc'] = tf.train.Saver(disc_params)
genm_dir = os.path.join( out_basedir, 'gen')
discm_dir = os.path.join( out_basedir, 'disc')
if configs.eval_only==False and args.sampling==False:
    os.makedirs(genm_dir)
    os.makedirs(discm_dir)

with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as session:
    #load a sample batch of data and output
    MnistB.init_iterator(session, train_dname)
    MnistB.init_iterator(session, dev_dname)
 
    if configs.eval_only==False and args.sampling==False:
        samples = session.run(real_data)
        tflib.save_images.save_images(
            samples.reshape((hparams.batch_size, DATA_LEN, DATA_LEN)), 
            os.path.join( out_basedir, 'real_data_samples.png'))
    #load pretrained model
    if configs.model_path and not configs.eval_only and not args.sampling: #start from training disc/gen
        # if configs.sep_saving_flag == False:
        #     cwd = os.getcwd()
        #     os.chdir(configs.model_path)
        #     savers['all'].restore(session,'model_snapshot')
        #     os.chdir(cwd)
        # else:
        cwd = os.getcwd()
        gmodel_path = os.path.join( configs.model_path, 'gen' )
        os.chdir(gmodel_path)
        savers['gen'].restore(session, 'model_snapshot')
        dmodel_path = os.path.join( configs.model_path, 'disc' )
        os.chdir(dmodel_path)
        savers['disc'].restore(session, 'model_snapshot')
        os.chdir(cwd)
        init_op = tf.variables_initializer(
        [v for v in tf.global_variables() if v.name.split(':')[0] in set(session.run(tf.report_uninitialized_variables()))
        ])
        session.run(init_op)
    else:
        session.run(tf.global_variables_initializer())

    if configs.eval_only == True: #evaluation with independent critc, not using now
        assert(configs.model_path)
        cwd = os.getcwd()
        gmodel_path = os.path.join( configs.model_path, 'gen' )
        assert(os.path.isdir(gmodel_path))
        os.chdir(gmodel_path)
        savers['gen'].restore(session, 'model_snapshot')        
        init_op = tf.variables_initializer(
        [v for v in tf.global_variables() if v.name.split(':')[0] in set(session.run(tf.report_uninitialized_variables()))
        ])
        session.run(init_op)   
        start = timer()  
        wdist_within_epoch = np.zeros((100))
        last_time_wdist = []
        for iteration in xrange(10000): #100 epochs
            ind_disc_logs = session.run(
                [ind_disc_cost, ind_disc_train_op, ind_wdist, ind_gradient_penalty])     
            wdist_within_epoch[iteration % 100] = ind_disc_logs[2]       
            if (iteration % 100 == 99): #1 epoch
                end = timer()
                txts = '{} it, time elapse = {:.3f}, '.format(iteration+1, end - start)
                txts += 'disc_cost = {:.3f}, w-dist_per_epoch = {:.3f} '.format(ind_disc_logs[0], np.mean(wdist_within_epoch))
                print(txts)
                start = end
                last_time_wdist.append( np.mean(wdist_within_epoch))
                if len(last_time_wdist) > 10:
                    last_time_wdist.pop(0)
        print('averge wdist over 10 epoch: {:.3f}'.format(np.mean(last_time_wdist)))
    elif args.sampling:
        assert(configs.model_path)
        cwd = os.getcwd()
        gmodel_path = configs.model_path
        os.chdir(gmodel_path)
        savers['gen'].restore(session, 'model_snapshot') 
        os.chdir(cwd)       
        init_op = tf.variables_initializer(
        [v for v in tf.global_variables() if v.name.split(':')[0] in set(session.run(tf.report_uninitialized_variables()))
        ])
        session.run(init_op) 
        for z in xrange(sampling_configs.sampling_rounds):
            tmp = []
            for i in xrange(sampling_configs.sampling_times):                                                                             
                tmp.append( (session.run(fake_data)*256).astype('uint8') )
            gen_imgs = np.concatenate(tmp)
            gen_imgs = np.reshape(gen_imgs, [-1, 28, 28, 1]) 
            gen_imgs = np.tile(gen_imgs, [1, 1, 1, 3]) 
            with open(os.path.join( out_basedir, '{}_{}.npy'.format(sampling_configs.sampling_outfile_name, z) ), 'w') as f:
                np.save(f, gen_imgs)        
    else:
        start = timer()
        avg_wdist_vec = np.zeros((100))
        for iteration in xrange(hparams.iters):
            if iteration > 0:
                #_data = train_gen().next()
                #c_g_var = generate_g_var(BATCH_SIZE, DROPOUT_FLAG, RETAIN_PROB)
                #feed_dict={real_data: _data, g_var: c_g_var}
                gen_logs = session.run([gen_train_op, gen_cost])
                #tflib.plot.plot( os.path.join(out_basedir, 'train gen cost'), logs[1] )
            for i in xrange(hparams.disc_iters):
                #_data = train_gen().next()
                #c_g_var = generate_g_var(BATCH_SIZE, DROPOUT_FLAG, RETAIN_PROB)
                #feed_dict={real_data: _data, g_var: c_g_var}
                disc_logs = session.run(
                [disc_cost, disc_train_op, wdist, gradient_penalty])
            avg_wdist_vec[iteration % 100] = disc_logs[2]
            if configs.eval_while_training and iteration % 10000 == 9999: 
                session.run(tf.variables_initializer(ind_optimizer.variables()))
                ind_init_op = tf.variables_initializer( ind_disc_params )
                session.run(ind_init_op)
                start = timer()  
                wdist_within_epoch = np.zeros((100))
                last_time_wdist = []
                for it in xrange(5000):
                    ind_disc_logs = session.run(
                        [ind_disc_train_op, ind_wdist])     
                    wdist_within_epoch[it % 100] = ind_disc_logs[1]       
                    if (it % 100 == 99): #1 epoch
                        last_time_wdist.append( np.mean(wdist_within_epoch))
                        if len(last_time_wdist) > 5:
                            last_time_wdist.pop(0)
                end = timer()
                print('eval on dev set: 50 epoch ({:.3f}s), averge wdist over 5 epoch: {:.3f}'.format(end-start, np.mean(last_time_wdist)))

            if iteration % 5000 == 4999:
                cwd = os.getcwd()
                os.chdir(genm_dir)
                savers['gen'].save(session, './model_snapshot')
                os.chdir(discm_dir)
                savers['disc'].save(session, './model_snapshot')
                os.chdir(cwd)                    
                    
            # Calculate dev (test) loss  every 5000 iteration
            # if iteration % 5000 == 4999:
            #     test_disc_costs = []
            #     for images in test_gen():
            #         c_g_var = generate_g_var(BATCH_SIZE)       
            #         logs = session.run(
            #             [disc_cost],
            #             feed_dict={real_data_input: images }
            #         )
            #         test_disc_costs.append(logs[0])
            #     tflib.plot.plot( os.path.join(root_res_dir, 'test disc cost'), np.mean(test_disc_costs))
            #     test_dist[test_cnt] = np.mean(test_disc_costs)
            #     test_cnt+=1
            if iteration % 1000 == 999:    
                generate_image(iteration)
            #if iteration % 2000 == 1999:
            #    gradient_vis_wrapper(iteration, CODE_SIZE)
            # Write logs every 100 iters
            if iteration % 100 == 99:
                end = timer()
                txts = '{} it, time elapse = {:.3f}, '.format(iteration+1, end - start)
                txts += 'w-dist = {:.3f}, disc_cost = {:.3f}, avg-w-dist = {:.3f}, '.format(disc_logs[2], disc_logs[0], np.mean(avg_wdist_vec))
                txts += 'gen_cost = {:.3f}'.format(gen_logs[1])
                print(txts)
                start = end
            tflib.plot.tick()
