# README #

## What is this repository for? ##

This repository is the codes for the work ["Composition and Decomposition of GANs" (submitted to UAI2019)](https://openreview.net/forum?id=SkxLKcg3U4)
## Platform ##

The code by-default works on Linux. If running on Windows, please specify the output path with "-output" option.

## General description of each script ##
1. *part_wgan.py*: the script to train part generators, 
	- **-setting** option takes the json file for a experiment setting.
	- **-output** option takes a folder path for a output (default is /home/<user_name>/<data_set_name>/<train_objective>/<time_stamp>).
	- **-sampling** option is a flag to indicate only sampling from a given model. The parameters related to this sampling are also given in a json file take by **-setting** option.
2. *cdc_wgan.py*: the script to train GANs under composition/decomposition framework. It has the same parameter semantics as *part_wgan.py*
3. *compute_fid.py*: the script to compute fid between real images and generated images. The code is adopted from [here](https://github.com/bioinf-jku/TTUR/blob/master/fid.py).  
	-- **-dataset** option takes the dataset name to be compared.  
	-- **-genpath** option takes the folder path of the generate images. Noted the images should be generated with the -sampling option from *part_wgan.py* or *cdc_wgan.py* so they have the same format. The scripts compute fid over each image file in this folder.  
	-- **-label** option takes the class label of the dataset. It makes fid computation on the specified class label of the dataset.
## Replicate the experiments ##
1. MNIST-BACKGROUND (MNIST-MB)  
	- Prerequisites for the experiments: composition operation (c), foreground generator (f), background generator (b). We train them by the following commands.  
	  ```
	  python cdc_wgan.py -setting configs_MB/cdcgan_MB_comp.json
	  ```  
	  ```
      python part_wgan.py -setting configs_MB/pgan_MB-f.json
	  ```  
	  ```
	  python part_wgan.py -setting configs_MB/pgan_MB-b.json
	  ```  
	  Or they can be donwloaded [here](http://cs.unc.edu/~ycharn/models/MNIST-MB_prerequisites.zip).  You can put all trained models under a folder names trained_models (default in the configs).
    - To run task1:  
	 ```
	  python cdc_wgan.py -setting configs_MB/cdcgan_MB_task1.json
	  ```
    - To run task2:  
	  ```
	  python cdc_wgan.py -setting configs_MB/cdcgan_MB_task2.json
	  ```
    - Task3 includes two variants,  1): given c, f, train decompostion operation (d) and b. 2): given c, b train d and f. To run task3:  
	  ```
	  python cdc_wgan.py -setting configs_MB/cdcgan_MB_task3_bg.json
	  ```  
	  ```
	  python cdc_wgan.py -setting configs_MB/cdcgan_MB_task3_fg.json
	  ```  
	  (training the model without cycle-consistency regularization)
	  ```
	  python cdc_wgan.py -setting configs_MB/cdcgan_noreg_MB_task3_fg.json
	  ```	  
	  
    - To run task4:  
	```
	python cdc_wgan.py -setting configs_MB/cdcgan_MB_task4.json
	```  
	
2. MNIST-BOUNDINGBOX (MNIST-BB)  
	- Prerequisites for the experiments: composition operation (c), foreground generator (f) of digit 1. We train them by the following commands.  
	  ```
	  python cdc_wgan.py -setting configs_BB/cdcgan_BB_comp.json
	  ```  
	  ```
      python part_wgan.py -setting configs_BB/pgan_BB_f_label1.json
	  ```   
	  Or they can be donwloaded [here](http://cs.unc.edu/~ycharn/models/MNIST-BB_prerequisites.zip). You can put all trained models under a folder names trained_models (default in the configs).
	- Chain learning:
		- step 1: given c, f (of digit 1), train d, b.  
		```
		python cdc_wgan.py -setting configs_BB/cdcgan_BB_chain_step1.json
		```
		- step2. given c, b (from previous step), train d, f (of digit 2).  
		```
		python cdc_wgan.py -setting configs_BB/cdcgan_BB_chain_step2.json
		```		
1. Fashion-MNIST-BACKGROUND (FMB)  
	- Prerequisites for the experiments: composition operation (c), foreground generaotr for label5, background generator (b). We train them by the following commands.  
	  ```
	  python cdc_wgan.py -setting configs_FMB/cdcgan_FMB_comp.json
	  ```  
	  ```
      python part_wgan.py -setting configs_FMB/pgan_FMB_f_label5.json
	  ```  
	  (Use the same command as in MNIST-MB)
	  ```
	  pgan_MB-bpython part_wgan.py -setting configs_MB/pgan_MB-b.json 
	  ```    
	  Or they can be donwloaded [here](http://cs.unc.edu/~ycharn/models/FMB_prerequisites.zip).  You can put all trained models under a folder names trained_models (default in the configs).
    - Task3 given c, b, train decompostion operation (d) and f:  
	  ```
	  python cdc_wgan.py -setting configs_FMB/cdcgan_FMB_exp3_fg.json
	  ```  
	  (training the model without cycle-consistency regularization)
	  ```
	  python cdc_wgan.py -setting configs_FMB/cdcgan_noreg_FMB_exp3_fg.json
	  ```  
	- Transfer learning: given c learned from MNIST-MB, and b, train decomposition operation d and f:  
		```
		python cdc_wgan.py -setting cdcgan_FMB_exp3_fg_transfer.json
		```  
	- Chain learning:
		- step 1: given c, f (of label 5, sandals), train d, b.  
		```
		python cdc_wgan.py -setting configs_FMB/cdcgan_FMB_exp_chain_1.json
		```
		- step2. given c, b (from previous step), train d, f (of label 0, t-shirts).  
		```
		python cdc_wgan.py -setting configs_FMB/cdcgan_FMB_exp_chain_2.json
		```		
